﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class CarStat1232 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cash",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    Money = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cash", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "StatyaDvijeniya",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatyaDvijeniya", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "FinanceOpeartion",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DepartmentID = table.Column<int>(nullable: true),
                    operationType = table.Column<int>(nullable: false),
                    ActBU = table.Column<string>(nullable: true),
                    ActDate = table.Column<DateTime>(nullable: false),
                    SummWithNDS = table.Column<float>(nullable: false),
                    NDS = table.Column<float>(nullable: false),
                    SummWithOutNDS = table.Column<float>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ContrAgentID = table.Column<int>(nullable: true),
                    statyaID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinanceOpeartion", x => x.ID);
                    table.ForeignKey(
                        name: "FK_FinanceOpeartion_ContrAgents_ContrAgentID",
                        column: x => x.ContrAgentID,
                        principalTable: "ContrAgents",
                        principalColumn: "ContrAgentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FinanceOpeartion_Departments_DepartmentID",
                        column: x => x.DepartmentID,
                        principalTable: "Departments",
                        principalColumn: "DepartmentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FinanceOpeartion_StatyaDvijeniya_statyaID",
                        column: x => x.statyaID,
                        principalTable: "StatyaDvijeniya",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FinanceOpeartion_ContrAgentID",
                table: "FinanceOpeartion",
                column: "ContrAgentID");

            migrationBuilder.CreateIndex(
                name: "IX_FinanceOpeartion_DepartmentID",
                table: "FinanceOpeartion",
                column: "DepartmentID");

            migrationBuilder.CreateIndex(
                name: "IX_FinanceOpeartion_statyaID",
                table: "FinanceOpeartion",
                column: "statyaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cash");

            migrationBuilder.DropTable(
                name: "FinanceOpeartion");

            migrationBuilder.DropTable(
                name: "StatyaDvijeniya");
        }
    }
}
