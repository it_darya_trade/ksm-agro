﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class asdffasdfasdf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CheckBU",
                table: "FinanceOpeartion",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "FinanceOpeartion",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "cashID",
                table: "FinanceOpeartion",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "toDO",
                table: "FinanceOpeartion",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FinanceOpeartion_ProductId",
                table: "FinanceOpeartion",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_FinanceOpeartion_cashID",
                table: "FinanceOpeartion",
                column: "cashID");

            migrationBuilder.AddForeignKey(
                name: "FK_FinanceOpeartion_Products_ProductId",
                table: "FinanceOpeartion",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FinanceOpeartion_Cash_cashID",
                table: "FinanceOpeartion",
                column: "cashID",
                principalTable: "Cash",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinanceOpeartion_Products_ProductId",
                table: "FinanceOpeartion");

            migrationBuilder.DropForeignKey(
                name: "FK_FinanceOpeartion_Cash_cashID",
                table: "FinanceOpeartion");

            migrationBuilder.DropIndex(
                name: "IX_FinanceOpeartion_ProductId",
                table: "FinanceOpeartion");

            migrationBuilder.DropIndex(
                name: "IX_FinanceOpeartion_cashID",
                table: "FinanceOpeartion");

            migrationBuilder.DropColumn(
                name: "CheckBU",
                table: "FinanceOpeartion");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "FinanceOpeartion");

            migrationBuilder.DropColumn(
                name: "cashID",
                table: "FinanceOpeartion");

            migrationBuilder.DropColumn(
                name: "toDO",
                table: "FinanceOpeartion");
        }
    }
}
