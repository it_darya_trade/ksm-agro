﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class CarStat12321 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinanceOpeartion_StatyaDvijeniya_statyaID",
                table: "FinanceOpeartion");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StatyaDvijeniya",
                table: "StatyaDvijeniya");

            migrationBuilder.RenameTable(
                name: "StatyaDvijeniya",
                newName: "StatyaDvijeniyas");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StatyaDvijeniyas",
                table: "StatyaDvijeniyas",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_FinanceOpeartion_StatyaDvijeniyas_statyaID",
                table: "FinanceOpeartion",
                column: "statyaID",
                principalTable: "StatyaDvijeniyas",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FinanceOpeartion_StatyaDvijeniyas_statyaID",
                table: "FinanceOpeartion");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StatyaDvijeniyas",
                table: "StatyaDvijeniyas");

            migrationBuilder.RenameTable(
                name: "StatyaDvijeniyas",
                newName: "StatyaDvijeniya");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StatyaDvijeniya",
                table: "StatyaDvijeniya",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_FinanceOpeartion_StatyaDvijeniya_statyaID",
                table: "FinanceOpeartion",
                column: "statyaID",
                principalTable: "StatyaDvijeniya",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
