﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class Complex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ContrAgents",
                columns: table => new
                {
                    ContrAgentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ContrAgent_Name = table.Column<string>(nullable: true),
                    ContrAgent_Inn = table.Column<string>(nullable: true),
                    ContrAgent_Kpp = table.Column<string>(nullable: true),
                    ContrAgent_Ogrn = table.Column<string>(nullable: true),
                    ContrAgent_Contacts = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContrAgents", x => x.ContrAgentID);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    DepartmentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DepartmentTitle = table.Column<string>(nullable: true),
                    DepartmentInn = table.Column<string>(nullable: true),
                    DepartmentKpp = table.Column<string>(nullable: true),
                    DepartmentOgrn = table.Column<string>(nullable: true),
                    DepartmentAddress = table.Column<string>(nullable: true),
                    DepartmentBank = table.Column<string>(nullable: true),
                    DepartmentBik = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.DepartmentID);
                });

            migrationBuilder.CreateTable(
                name: "Pages",
                columns: table => new
                {
                    PageID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PageTitle = table.Column<string>(nullable: true),
                    PageUrl = table.Column<string>(nullable: true),
                    PageDescription = table.Column<string>(nullable: true),
                    PageImage = table.Column<string>(nullable: true),
                    PageSource = table.Column<string>(nullable: true),
                    PageKeywords = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pages", x => x.PageID);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductTitle = table.Column<string>(nullable: true),
                    ProductPrice = table.Column<double>(type: "float", nullable: false),
                    ProductsPriceWithNDS = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Login = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    RegID = table.Column<int>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    UserRole = table.Column<int>(nullable: false),
                    UserDepartment = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_Users_Departments_UserDepartment",
                        column: x => x.UserDepartment,
                        principalTable: "Departments",
                        principalColumn: "DepartmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Deals",
                columns: table => new
                {
                    DealID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ParentDealID = table.Column<int>(nullable: false),
                    ChildNumber = table.Column<int>(nullable: false),
                    dealStatus = table.Column<int>(nullable: false),
                    DealType = table.Column<int>(nullable: false),
                    DealDS = table.Column<DateTime>(nullable: false),
                    DepartmentID = table.Column<int>(nullable: true),
                    UserDeals = table.Column<int>(nullable: true),
                    ProductId = table.Column<int>(nullable: true),
                    ShipAddress = table.Column<string>(nullable: true),
                    SelledIdContrAgentID = table.Column<int>(nullable: true),
                    ConsumerIdContrAgentID = table.Column<int>(nullable: true),
                    ShipperIdContrAgentID = table.Column<int>(nullable: true),
                    Shippername = table.Column<string>(nullable: true),
                    Carname = table.Column<string>(nullable: true),
                    Rasstoyanie = table.Column<float>(nullable: false),
                    Price = table.Column<float>(nullable: false),
                    ShipPrice = table.Column<float>(nullable: false),
                    DonePrice = table.Column<float>(nullable: false),
                    BuyPriceWithNDSForOneKG = table.Column<float>(nullable: false),
                    SellPriceWithNDSForOneKG = table.Column<float>(nullable: false),
                    ShipPriceWithNDSForOneKG = table.Column<float>(nullable: false),
                    SafePriceForOneKG = table.Column<float>(nullable: false),
                    SellPriceNDS = table.Column<float>(nullable: false),
                    BuyPriceNDS = table.Column<float>(nullable: false),
                    ShipPriceNDS = table.Column<float>(nullable: false),
                    SafePriceNDS = table.Column<float>(nullable: false),
                    MoneyNDS = table.Column<float>(nullable: false),
                    DeltaShipping = table.Column<float>(nullable: false),
                    DeltaProshenaya = table.Column<float>(nullable: false),
                    SummDeltaShipp = table.Column<float>(nullable: false),
                    DeltaCompinsation = table.Column<int>(nullable: false),
                    DeltaPrice = table.Column<float>(nullable: false),
                    SubmitDelta = table.Column<float>(nullable: false),
                    transoprtScheme = table.Column<int>(nullable: false),
                    ShipPriceWithNDS = table.Column<float>(nullable: false),
                    ShipSancForNum = table.Column<float>(nullable: false),
                    TransportdopRaschod = table.Column<float>(nullable: false),
                    transoprtSummWithNDS = table.Column<float>(nullable: false),
                    transoprtShipNDS = table.Column<float>(nullable: false),
                    transoprtShipWithOutNDS = table.Column<float>(nullable: false),
                    ACTBUNumber = table.Column<string>(nullable: true),
                    ACTBUDate = table.Column<DateTime>(nullable: false),
                    TTNType = table.Column<string>(nullable: true),
                    transoprtPayWithNDS = table.Column<float>(nullable: false),
                    buyScheme = table.Column<int>(nullable: false),
                    buyWithNDS = table.Column<float>(nullable: false),
                    buySancForLost = table.Column<int>(nullable: false),
                    buyWithCompensation = table.Column<float>(nullable: false),
                    withOutNDS = table.Column<float>(nullable: false),
                    buyNDS = table.Column<float>(nullable: false),
                    buyActBU = table.Column<string>(nullable: true),
                    buyActDate = table.Column<DateTime>(nullable: false),
                    buySummWithNDS = table.Column<float>(nullable: false),
                    cSNumber = table.Column<string>(nullable: true),
                    cSShipPoint = table.Column<string>(nullable: true),
                    cSOtgruz = table.Column<float>(nullable: false),
                    cSOneKGPriceWithoutNDS = table.Column<float>(nullable: false),
                    cSSumm = table.Column<float>(nullable: false),
                    safeContrContrAgentID = table.Column<int>(nullable: true),
                    safePriceWithNDS = table.Column<string>(nullable: true),
                    safeNDSSUMM = table.Column<float>(nullable: false),
                    safeSUMMWithOutNDS = table.Column<float>(nullable: false),
                    safeActBU = table.Column<string>(nullable: true),
                    safeActDate = table.Column<DateTime>(nullable: false),
                    raschAgentskije = table.Column<float>(nullable: false),
                    raschProchee = table.Column<float>(nullable: false),
                    raschSummAll = table.Column<float>(nullable: false),
                    prodWithNDS = table.Column<float>(nullable: false),
                    prodWithOutNDS = table.Column<float>(nullable: false),
                    prodNDSSumm = table.Column<float>(nullable: false),
                    prodDogovor = table.Column<string>(nullable: true),
                    prodActBU = table.Column<string>(nullable: true),
                    prodActDate = table.Column<DateTime>(nullable: false),
                    prodPayWithNDS = table.Column<float>(nullable: false),
                    nakRaschWithOutNDS = table.Column<float>(nullable: false),
                    nakRaschWithOurRaspred = table.Column<float>(nullable: false),
                    nakRasch = table.Column<float>(nullable: false),
                    nakRaschSUMM = table.Column<float>(nullable: false),
                    marjPribyl = table.Column<float>(nullable: false),
                    marjiNalnost = table.Column<float>(nullable: false),
                    marjaOneKg = table.Column<float>(nullable: false),
                    prybDoNP = table.Column<float>(nullable: false),
                    nalogNP = table.Column<float>(nullable: false),
                    chistPryb = table.Column<float>(nullable: false),
                    chistPrybOneKG = table.Column<float>(nullable: false),
                    chistPrybRenta = table.Column<float>(nullable: false),
                    poteriPriDostWithOutCompensation = table.Column<float>(nullable: false),
                    poteriPribPriDostWithOutCompensation = table.Column<float>(nullable: false),
                    poteriDostavPrijem = table.Column<float>(nullable: false),
                    poteriPryb = table.Column<float>(nullable: false),
                    chistPrybnedPryb = table.Column<float>(nullable: false),
                    chistPrybOneKGFinish = table.Column<float>(nullable: false),
                    finishRenta = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deals", x => x.DealID);
                    table.ForeignKey(
                        name: "FK_Deals_ContrAgents_ConsumerIdContrAgentID",
                        column: x => x.ConsumerIdContrAgentID,
                        principalTable: "ContrAgents",
                        principalColumn: "ContrAgentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deals_Departments_DepartmentID",
                        column: x => x.DepartmentID,
                        principalTable: "Departments",
                        principalColumn: "DepartmentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deals_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deals_ContrAgents_SelledIdContrAgentID",
                        column: x => x.SelledIdContrAgentID,
                        principalTable: "ContrAgents",
                        principalColumn: "ContrAgentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deals_ContrAgents_ShipperIdContrAgentID",
                        column: x => x.ShipperIdContrAgentID,
                        principalTable: "ContrAgents",
                        principalColumn: "ContrAgentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deals_Users_UserDeals",
                        column: x => x.UserDeals,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Deals_ContrAgents_safeContrContrAgentID",
                        column: x => x.safeContrContrAgentID,
                        principalTable: "ContrAgents",
                        principalColumn: "ContrAgentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    PostID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PostTitle = table.Column<string>(nullable: true),
                    PostImage = table.Column<string>(nullable: true),
                    PostDescription = table.Column<string>(nullable: true),
                    PostText = table.Column<string>(nullable: true),
                    PostDS = table.Column<DateTime>(nullable: false),
                    PostKeyWords = table.Column<string>(nullable: true),
                    PostMetaDesc = table.Column<string>(nullable: true),
                    UserPosts = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.PostID);
                    table.ForeignKey(
                        name: "FK_Posts_Users_UserPosts",
                        column: x => x.UserPosts,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deals_ConsumerIdContrAgentID",
                table: "Deals",
                column: "ConsumerIdContrAgentID");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_DepartmentID",
                table: "Deals",
                column: "DepartmentID");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_ProductId",
                table: "Deals",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_SelledIdContrAgentID",
                table: "Deals",
                column: "SelledIdContrAgentID");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_ShipperIdContrAgentID",
                table: "Deals",
                column: "ShipperIdContrAgentID");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_UserDeals",
                table: "Deals",
                column: "UserDeals");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_safeContrContrAgentID",
                table: "Deals",
                column: "safeContrContrAgentID");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_UserPosts",
                table: "Posts",
                column: "UserPosts");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserDepartment",
                table: "Users",
                column: "UserDepartment",
                unique: true,
                filter: "[UserDepartment] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Deals");

            migrationBuilder.DropTable(
                name: "Pages");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "ContrAgents");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Departments");
        }
    }
}
