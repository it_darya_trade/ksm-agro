﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class Status12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DealType",
                table: "Deals");

            migrationBuilder.AddColumn<int>(
                name: "DealTypeId",
                table: "Deals",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DealType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deals_DealTypeId",
                table: "Deals",
                column: "DealTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_DealType_DealTypeId",
                table: "Deals",
                column: "DealTypeId",
                principalTable: "DealType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deals_DealType_DealTypeId",
                table: "Deals");

            migrationBuilder.DropTable(
                name: "DealType");

            migrationBuilder.DropIndex(
                name: "IX_Deals_DealTypeId",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "DealTypeId",
                table: "Deals");

            migrationBuilder.AddColumn<int>(
                name: "DealType",
                table: "Deals",
                nullable: false,
                defaultValue: 0);
        }
    }
}
