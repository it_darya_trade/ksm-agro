﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class DealInfo2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "dealTypeId",
                table: "DealInfo",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DealInfo_dealTypeId",
                table: "DealInfo",
                column: "dealTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealInfo_DealTypes_dealTypeId",
                table: "DealInfo",
                column: "dealTypeId",
                principalTable: "DealTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealInfo_DealTypes_dealTypeId",
                table: "DealInfo");

            migrationBuilder.DropIndex(
                name: "IX_DealInfo_dealTypeId",
                table: "DealInfo");

            migrationBuilder.DropColumn(
                name: "dealTypeId",
                table: "DealInfo");
        }
    }
}
