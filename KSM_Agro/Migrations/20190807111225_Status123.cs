﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class Status123 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deals_DealType_DealTypeId",
                table: "Deals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealType",
                table: "DealType");

            migrationBuilder.RenameTable(
                name: "DealType",
                newName: "DealTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealTypes",
                table: "DealTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_DealTypes_DealTypeId",
                table: "Deals",
                column: "DealTypeId",
                principalTable: "DealTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deals_DealTypes_DealTypeId",
                table: "Deals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DealTypes",
                table: "DealTypes");

            migrationBuilder.RenameTable(
                name: "DealTypes",
                newName: "DealType");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DealType",
                table: "DealType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_DealType_DealTypeId",
                table: "Deals",
                column: "DealTypeId",
                principalTable: "DealType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
