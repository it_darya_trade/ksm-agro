﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class newDeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "nakRaschWithOurRaspred",
                table: "Deals",
                newName: "nakRaschOfDepartmentWithOutNP");

            migrationBuilder.AddColumn<string>(
                name: "Season",
                table: "FinanceOpeartion",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "MPMinusNDSNP",
                table: "Deals",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<string>(
                name: "Seasons",
                table: "Deals",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "UKZatraty",
                table: "Deals",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "bazaRaspredByDepartment",
                table: "Deals",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "bazaRaspredelenijaAllDeals",
                table: "Deals",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "marjaOneKgNP",
                table: "Deals",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "marjiNalnostNP",
                table: "Deals",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Season",
                table: "FinanceOpeartion");

            migrationBuilder.DropColumn(
                name: "MPMinusNDSNP",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "Seasons",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "UKZatraty",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "bazaRaspredByDepartment",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "bazaRaspredelenijaAllDeals",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "marjaOneKgNP",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "marjiNalnostNP",
                table: "Deals");

            migrationBuilder.RenameColumn(
                name: "nakRaschOfDepartmentWithOutNP",
                table: "Deals",
                newName: "nakRaschWithOurRaspred");
        }
    }
}
