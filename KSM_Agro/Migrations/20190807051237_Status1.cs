﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class Status1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "dealStatus",
                table: "Deals");

            migrationBuilder.AddColumn<int>(
                name: "dealStatusID",
                table: "Deals",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DealStatus",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealStatus", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Deals_dealStatusID",
                table: "Deals",
                column: "dealStatusID");

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_DealStatus_dealStatusID",
                table: "Deals",
                column: "dealStatusID",
                principalTable: "DealStatus",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deals_DealStatus_dealStatusID",
                table: "Deals");

            migrationBuilder.DropTable(
                name: "DealStatus");

            migrationBuilder.DropIndex(
                name: "IX_Deals_dealStatusID",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "dealStatusID",
                table: "Deals");

            migrationBuilder.AddColumn<int>(
                name: "dealStatus",
                table: "Deals",
                nullable: false,
                defaultValue: 0);
        }
    }
}
