﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class CarStat123211 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CFStruct",
                table: "StatyaDvijeniyas",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CFType",
                table: "StatyaDvijeniyas",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PLStruct",
                table: "StatyaDvijeniyas",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PLtype",
                table: "StatyaDvijeniyas",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BuyerCashID",
                table: "Deals",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SellerCashID",
                table: "Deals",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ShipperCashID",
                table: "Deals",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deals_BuyerCashID",
                table: "Deals",
                column: "BuyerCashID");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_SellerCashID",
                table: "Deals",
                column: "SellerCashID");

            migrationBuilder.CreateIndex(
                name: "IX_Deals_ShipperCashID",
                table: "Deals",
                column: "ShipperCashID");

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_Cash_BuyerCashID",
                table: "Deals",
                column: "BuyerCashID",
                principalTable: "Cash",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_Cash_SellerCashID",
                table: "Deals",
                column: "SellerCashID",
                principalTable: "Cash",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_Cash_ShipperCashID",
                table: "Deals",
                column: "ShipperCashID",
                principalTable: "Cash",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deals_Cash_BuyerCashID",
                table: "Deals");

            migrationBuilder.DropForeignKey(
                name: "FK_Deals_Cash_SellerCashID",
                table: "Deals");

            migrationBuilder.DropForeignKey(
                name: "FK_Deals_Cash_ShipperCashID",
                table: "Deals");

            migrationBuilder.DropIndex(
                name: "IX_Deals_BuyerCashID",
                table: "Deals");

            migrationBuilder.DropIndex(
                name: "IX_Deals_SellerCashID",
                table: "Deals");

            migrationBuilder.DropIndex(
                name: "IX_Deals_ShipperCashID",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "CFStruct",
                table: "StatyaDvijeniyas");

            migrationBuilder.DropColumn(
                name: "CFType",
                table: "StatyaDvijeniyas");

            migrationBuilder.DropColumn(
                name: "PLStruct",
                table: "StatyaDvijeniyas");

            migrationBuilder.DropColumn(
                name: "PLtype",
                table: "StatyaDvijeniyas");

            migrationBuilder.DropColumn(
                name: "BuyerCashID",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "SellerCashID",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "ShipperCashID",
                table: "Deals");
        }
    }
}
