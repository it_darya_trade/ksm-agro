﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class DealInfo1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GUID",
                table: "DealInfo",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "MoneyNDS",
                table: "DealInfo",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "SellPriceNDS",
                table: "DealInfo",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "ShipPriceNDS",
                table: "DealInfo",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "buyPriceNDS",
                table: "DealInfo",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GUID",
                table: "DealInfo");

            migrationBuilder.DropColumn(
                name: "MoneyNDS",
                table: "DealInfo");

            migrationBuilder.DropColumn(
                name: "SellPriceNDS",
                table: "DealInfo");

            migrationBuilder.DropColumn(
                name: "ShipPriceNDS",
                table: "DealInfo");

            migrationBuilder.DropColumn(
                name: "buyPriceNDS",
                table: "DealInfo");
        }
    }
}
