﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class DealInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deals_DealStatus_dealStatusID",
                table: "Deals");

            migrationBuilder.DropIndex(
                name: "IX_Deals_dealStatusID",
                table: "Deals");

            migrationBuilder.DropColumn(
                name: "dealStatusID",
                table: "Deals");

            migrationBuilder.CreateTable(
                name: "DealInfo",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dealStatusID = table.Column<int>(nullable: true),
                    Season = table.Column<string>(nullable: true),
                    CreatorUserId = table.Column<int>(nullable: true),
                    ProductId = table.Column<int>(nullable: true),
                    DealDS = table.Column<DateTime>(nullable: false),
                    DepartmentID = table.Column<int>(nullable: true),
                    UID = table.Column<string>(nullable: true),
                    DealTimeLong = table.Column<int>(nullable: false),
                    Price = table.Column<float>(nullable: false),
                    DonePrice = table.Column<float>(nullable: false),
                    ShipPrice = table.Column<float>(nullable: false),
                    DeltaShipping = table.Column<float>(nullable: false),
                    SummDeltaShipp = table.Column<float>(nullable: false),
                    SubmitDelta = table.Column<float>(nullable: false),
                    TransportdopRaschod = table.Column<float>(nullable: false),
                    transoprtSummWithNDS = table.Column<float>(nullable: false),
                    transoprtShipNDS = table.Column<float>(nullable: false),
                    transoprtShipWithOutNDS = table.Column<float>(nullable: false),
                    transoprtPayWithNDS = table.Column<float>(nullable: false),
                    buyWithNDS = table.Column<float>(nullable: false),
                    buySancForLost = table.Column<float>(nullable: false),
                    buyWithCompensation = table.Column<float>(nullable: false),
                    buyNDS = table.Column<float>(nullable: false),
                    withOutNDS = table.Column<float>(nullable: false),
                    buySummWithNDS = table.Column<float>(nullable: false),
                    cSOtgruz = table.Column<float>(nullable: false),
                    cSOneKGPriceWithoutNDS = table.Column<float>(nullable: false),
                    cSSumm = table.Column<float>(nullable: false),
                    safePriceWithNDS = table.Column<float>(nullable: false),
                    SafePriceNDS = table.Column<float>(nullable: false),
                    safeSUMMWithOutNDS = table.Column<float>(nullable: false),
                    raschAgentskije = table.Column<float>(nullable: false),
                    raschProchee = table.Column<float>(nullable: false),
                    raschSummAll = table.Column<float>(nullable: false),
                    prodWithNDS = table.Column<float>(nullable: false),
                    prodNDSSumm = table.Column<float>(nullable: false),
                    prodWithOutNDS = table.Column<float>(nullable: false),
                    prodPayWithNDS = table.Column<float>(nullable: false),
                    nakRaschWithOutNDS = table.Column<float>(nullable: false),
                    nakRaschOfDepartmentWithOutNP = table.Column<float>(nullable: false),
                    nakRasch = table.Column<float>(nullable: false),
                    nakRaschSUMM = table.Column<float>(nullable: false),
                    marjPribyl = table.Column<float>(nullable: false),
                    marjiNalnost = table.Column<float>(nullable: false),
                    marjaOneKg = table.Column<float>(nullable: false),
                    MPMinusNDSNP = table.Column<float>(nullable: false),
                    marjiNalnostNP = table.Column<float>(nullable: false),
                    marjaOneKgNP = table.Column<float>(nullable: false),
                    prybDoNP = table.Column<float>(nullable: false),
                    nalogNP = table.Column<float>(nullable: false),
                    chistPryb = table.Column<float>(nullable: false),
                    chistPrybOneKG = table.Column<float>(nullable: false),
                    chistPrybRenta = table.Column<float>(nullable: false),
                    poteriPriDostWithOutCompensation = table.Column<float>(nullable: false),
                    poteriPribPriDostWithOutCompensation = table.Column<float>(nullable: false),
                    poteriPryb = table.Column<float>(nullable: false),
                    poteriDostavPrijem = table.Column<float>(nullable: false),
                    chistPrybnedPryb = table.Column<float>(nullable: false),
                    chistPrybOneKGFinish = table.Column<float>(nullable: false),
                    finishRenta = table.Column<float>(nullable: false),
                    BuyPriceWithNDSForOneKG = table.Column<float>(nullable: false),
                    ShipPriceWithNDSForOneKG = table.Column<float>(nullable: false),
                    SellPriceWithNDSForOneKG = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealInfo", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DealInfo_Users_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealInfo_Departments_DepartmentID",
                        column: x => x.DepartmentID,
                        principalTable: "Departments",
                        principalColumn: "DepartmentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealInfo_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealInfo_DealStatus_dealStatusID",
                        column: x => x.dealStatusID,
                        principalTable: "DealStatus",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DealInfo_CreatorUserId",
                table: "DealInfo",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DealInfo_DepartmentID",
                table: "DealInfo",
                column: "DepartmentID");

            migrationBuilder.CreateIndex(
                name: "IX_DealInfo_ProductId",
                table: "DealInfo",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_DealInfo_dealStatusID",
                table: "DealInfo",
                column: "dealStatusID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DealInfo");

            migrationBuilder.AddColumn<int>(
                name: "dealStatusID",
                table: "Deals",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Deals_dealStatusID",
                table: "Deals",
                column: "dealStatusID");

            migrationBuilder.AddForeignKey(
                name: "FK_Deals_DealStatus_dealStatusID",
                table: "Deals",
                column: "dealStatusID",
                principalTable: "DealStatus",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
