﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class Complex1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DealUID",
                table: "Deals",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DealUID",
                table: "Deals");
        }
    }
}
