﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KSM_Agro.Migrations
{
    public partial class newDeal1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Seasons",
                table: "Deals",
                newName: "Season");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Season",
                table: "Deals",
                newName: "Seasons");
        }
    }
}
