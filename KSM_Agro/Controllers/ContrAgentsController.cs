﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KSM_Agro.Models;

namespace KSM_Agro.Controllers
{
    public class ContrAgentsController : Controller
    {
        private readonly DataBaseContext _context;

        public ContrAgentsController(DataBaseContext context)
        {
            _context = context;
        }

        // GET: ContrAgents
        [HttpGet("/panel/clients/")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.ContrAgents.OrderBy(p=>p.ContrAgent_Name).Where(p => p.ContrAgent_Name!=null).ToListAsync());
        }

        // GET: ContrAgents/Details/5
        [HttpGet("/panel/clients/{id?}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contrAgent = await _context.ContrAgents
                .FirstOrDefaultAsync(m => m.ContrAgentID == id);
            if (contrAgent == null)
            {
                return NotFound();
            }

            return View(contrAgent);
        }

        // GET: ContrAgents/Create
        [HttpGet("/panel/clients/new")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: ContrAgents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/panel/clients/new")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ContrAgentID,ContrAgent_Name,ContrAgent_Inn,ContrAgent_Kpp,ContrAgent_Ogrn,ContrAgent_Contacts")] ContrAgent contrAgent)
        {
            if (ModelState.IsValid)
            {
                _context.Add(contrAgent);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(contrAgent);
        }

        // GET: ContrAgents/Edit/5
        [HttpGet("/panel/clients/edit/{id?}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contrAgent = await _context.ContrAgents.FindAsync(id);
            if (contrAgent == null)
            {
                return NotFound();
            }
            return View(contrAgent);
        }

        // POST: ContrAgents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/panel/clients/edit/{id?}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ContrAgentID,ContrAgent_Name,ContrAgent_Inn,ContrAgent_Kpp,ContrAgent_Ogrn,ContrAgent_Contacts")] ContrAgent contrAgent)
        {
            if (id != contrAgent.ContrAgentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(contrAgent);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContrAgentExists(contrAgent.ContrAgentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(contrAgent);
        }

        // GET: ContrAgents/Delete/5
        [HttpGet("/panel/clients/delete/{id?}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contrAgent = await _context.ContrAgents
                .FirstOrDefaultAsync(m => m.ContrAgentID == id);
            if (contrAgent == null)
            {
                return NotFound();
            }

            return View(contrAgent);
        }

        // POST: ContrAgents/Delete/5
        [HttpPost("/panel/clients/delete/{id?}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var contrAgent = await _context.ContrAgents.FindAsync(id);
            _context.ContrAgents.Remove(contrAgent);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContrAgentExists(int id)
        {
            return _context.ContrAgents.Any(e => e.ContrAgentID == id);
        }
    }
}
