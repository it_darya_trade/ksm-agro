﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Mvc;
using MimeKit;

namespace KSM_Agro.Controllers
{
    public class EmailController
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "noreply@ksm-agro.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = "<!DOCTYPE html>" +
                "<html>" +
                "<head>" +
                "<title>"+ subject + "</title>" +
                "</head>" +
                "<body>" +
                "<table>" +
                "<tbody>" +
                "<tr>" +
                "<td style='display: flex; justify-content: center;'>" +
                "<div style=\"border: 4px solid #00ADB5\">" +
"                    <a href=\"https://ksm-agro.com\" ><h1 style=\"font-size: 32px; margin: 10px\">КСМ</h1></a>" +
"                </div>" +
                "</td>" +
                "</tr>" +
                "</tbody>" +
                "<table>" + message +
                "</body>" +
                "</html>"
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.jino.ru", 587, false);
                await client.AuthenticateAsync("noreply@ksm-agro.com", "28jLp#jGtmxj");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}