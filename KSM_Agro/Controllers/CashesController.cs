﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KSM_Agro.Models;

namespace KSM_Agro.Controllers
{
    public class CashesController : Controller
    {
        private readonly DataBaseContext _context;

        public CashesController(DataBaseContext context)
        {
            _context = context;
        }

        [HttpGet("/panel/cash")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Cash.ToListAsync());
        }

        [HttpGet("/panel/cash/{id?}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cash = await _context.Cash
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cash == null)
            {
                return NotFound();
            }

            return View(cash);
        }

        [HttpGet("/panel/cash/create")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Cashes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/panel/cash/create")]
        public async Task<IActionResult> Create([Bind("ID,Title,Money")] Cash cash)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cash);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(cash);
        }

        [HttpGet("/panel/cash/edit/{id?}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cash = await _context.Cash.FindAsync(id);
            if (cash == null)
            {
                return NotFound();
            }
            return View(cash);
        }

        // POST: Cashes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/panel/cash/edit/{id?}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Money")] Cash cash)
        {
            if (id != cash.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cash);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CashExists(cash.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(cash);
        }

        [HttpGet("/panel/cash/delete/{id?}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cash = await _context.Cash
                .FirstOrDefaultAsync(m => m.ID == id);
            if (cash == null)
            {
                return NotFound();
            }

            return View(cash);
        }

        // POST: Cashes/Delete/5
        [HttpPost("/panel/cash/delete/{id?}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cash = await _context.Cash.FindAsync(id);
            _context.Cash.Remove(cash);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CashExists(int id)
        {
            return _context.Cash.Any(e => e.ID == id);
        }
    }
}
