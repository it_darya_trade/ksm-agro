﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using KSM_Agro.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;


namespace KSM_Agro.Controllers
{
    public class AuthController : Controller
    {
        private DataBaseContext db;
        public AuthController(DataBaseContext context)
        {
            db = context;
        }


        [HttpGet("/login")]
        public IActionResult Login()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                
                return RedirectPermanent("/panel");
            }
            else
            {
                return View();
            }
        }


        [HttpPost("/login")]
        public async Task<IActionResult> Login(string login, string password)
        {
            User user = await db.Users.FirstOrDefaultAsync(u => u.Login == login && u.Password == SHA256STR(password));
            if (user != null)
            {
                await Authenticate(user);
                ViewBag.UserName = user.Name;
                HttpContext.Session.SetString("User", JsonConvert.SerializeObject(user));
                return RedirectPermanent("/panel");
                //return View("Views/Home/Loading.cshtml");

            }
            else return View();

        }

        [HttpGet("/register")]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost("/register")]
        public IActionResult Register(string name, string lastname, string login, string email, string password, string phone)
        {
            User user = db.Users.FirstOrDefault(u => u.Login == login);
            if (user == null)
            {
                string pass = SHA256STR(password);
                User usr = new User { Name = name, LastName = lastname, Login = login, Email = email, Password = pass, Phone = phone, RegID = 99, UserRole = 4 };
                db.Users.Add(usr);
                db.SaveChanges();
                _ = Authenticate(usr);

                return RedirectPermanent("/");
            }
            else
                return View();
        }


        [HttpGet("/register/{token?}")]
        public IActionResult RegisterByToken(string token) {
            User us = db.Users.FirstOrDefault(u => u.Token == token);
            if (us != null)
            {
                ViewBag.Token = us.Token;
                HttpContext.Session.SetString("Token", us.Token);
                return View("/Views/Auth/CreatePwd.cshtml");
            }
            else
            {
                return Redirect("/register");
            }
        }

        [HttpPost("/register/{token?}"), ActionName("CreatePwd")]
        public IActionResult RegisterByTokenPost(string password, string secpassword)
        {
            if (password == secpassword)
            {
                User user = db.Users.FirstOrDefault(u => u.Token == HttpContext.Session.GetString("Token"));
                user.Password = SHA256STR(password);
                user.Token = "";
                db.Users.Update(user);
                db.SaveChanges();
                return RedirectPermanent("/");
            }
            else
            {
                return View();
            }
        }


        private async Task Authenticate(User user)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserId.ToString()),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.UserRole.ToString())
        };

            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }


        [HttpGet("/logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect("/login");
        }


        public string SHA256STR(string data)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}