﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KSM_Agro.Models;
using System.Xml.Linq;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Routing;
using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace KSM_Agro.Controllers
{
    public class HomeController : Controller
    {
        private DataBaseContext pCtx;
        private readonly IHostingEnvironment _appEnvironment;
        public HomeController(DataBaseContext _pCtx, IHostingEnvironment appEnvironment) {
            pCtx = _pCtx;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index()
        {
            ViewBag.Posts = pCtx.Posts.OrderByDescending(p => p.PostDS).Take(4).ToArray();
            if (HttpContext.User.Identity.IsAuthenticated) {
                ViewBag.Name = HttpContext.User.Identity.Name;
                return View("/Views/Home/Index.cshtml");
            } else return View("/Views/Home/Index.cshtml");
        }
        [HttpGet("/index")]
        public IActionResult Index1()
        {
            ViewBag.Posts = pCtx.Posts.OrderByDescending(p => p.PostDS).Take(4).ToArray();
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                ViewBag.Name = HttpContext.User.Identity.Name;
                return View("/Views/Home/Index.cshtml");
            }
            else return View("/Views/Home/Index.cshtml");
        }
        [HttpGet("/index.html")]
        public IActionResult Index2()
        {
            ViewBag.Posts = pCtx.Posts.OrderByDescending(p => p.PostDS).Take(4).ToArray();
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                ViewBag.Name = HttpContext.User.Identity.Name;
                return View("/Views/Home/Index.cshtml");
            }
            else return View("/Views/Home/Index.cshtml");
        }

        [HttpGet("/index.php")]
        public IActionResult Index3()
        {
            ViewBag.Posts = pCtx.Posts.OrderByDescending(p => p.PostDS).Take(4).ToArray();
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                ViewBag.Name = HttpContext.User.Identity.Name;
                return View("/Views/Home/Index.cshtml");
            }
            else return View("/Views/Home/Index.cshtml");
        }



        [HttpGet("/about")]
        [Route("/about.html")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        [HttpGet("/contacts")]
        [Route("/contacts.html")]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpGet("/privacy")]
        [Route("/privacy.html")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet("/services")]
        [Route("/services.html")]
        public IActionResult Services()
        {
            return View();
        }

        [HttpGet("/products")]
        [Route("/products.html")]
        public IActionResult Products()
        {
            return View();
        }

        [HttpGet("/docs")]
        [Route("/docs.html")]
        public IActionResult Docs()
        {
            return View();
        }

        [HttpGet("/employers")]
        [Route("/employers.html")]
        public IActionResult Employers()
        {

            return View();
        }

        [HttpGet("/tech")]
        [Route("/tech.html")]
        public IActionResult Tech()
        {
            return View();
        }

        [HttpGet("/news")]
        [Route("/news.html")]
        public IActionResult News()
        {
            ViewBag.Posts = pCtx.Posts.OrderByDescending(p => p.PostDS).Take(10).ToArray();
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                ViewBag.Name = HttpContext.User.Identity.Name;
                return View();
            }
            else return View();
        }

        [HttpGet("/news/read/{id?}")]
        public IActionResult ReadPost(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post =  pCtx.Posts
                .FirstOrDefault(m => m.PostID == id);
            if (post == null)
            {
                return NotFound();
            }
            else
            {

            }
            ViewBag.Post = post;
            return View("Views/Home/PostRead.cshtml");
        }


        [HttpGet("/projects")]
        [Route("/projects.html")]
        public IActionResult Projects()
        {
            return View();
        }

        [Route("/error/{code:int}")]
        public IActionResult Error(int code)
        {
            ViewBag.Code = code;
            return View();
        }

        [HttpGet("/upload")]
        public IActionResult Upload(IFormFile file)
        {
            Console.WriteLine(HttpContext.Request.Query.Count);
            string url = new FileUploadController().FileUploader(file);
            return Content("<script>window.parent.CKEDITOR.tools.callFunction(" + "CKEditorFuncNum" + ", \"" + url + "\");</script>");
        }

        [HttpGet("services/building")]
        [Route("services/building.html")]
        public IActionResult building()
        {
            return View();
        }
        [HttpGet("services/demolition")]
        [Route("services/demolition.html")]
        public IActionResult demolition()
        {
            return View();
        }
        [HttpGet("services/ved")]
        [Route("services/ved.html")]
        public IActionResult ved()
        {
            return View();
        }
        [HttpGet("services/logistics")]
        [Route("services/logistics.html")]
        public IActionResult logistics()
        {
            return View();
        }

        [HttpGet("/docs/{file?}")]
        public IActionResult GetDocs(string file)
        {
            string file_path = Path.Combine(_appEnvironment.ContentRootPath, "wwwroot/files/"+file);
            return PhysicalFile(file_path, "application/pdf");
        }



        [HttpGet("/sitemap.xml")]
        public IActionResult GetSiteMap(string file)
        {
            string file_path = Path.Combine(_appEnvironment.ContentRootPath, "wwwroot/files/sitemap.xml");
            return PhysicalFile(file_path, "type/xml");
        }

        [HttpGet("/robots.txt")]
        public IActionResult GetRobots(string file)
        {
            string file_path = Path.Combine(_appEnvironment.ContentRootPath, "wwwroot/robots.txt");
            return PhysicalFile(file_path, "type/plain");
        }

        [HttpPost("/answer")]
        public async Task<IActionResult> GetAnswer(string name, string phone, string text, string email)
        {
            Console.WriteLine(name);
            Console.WriteLine(phone);
            Console.WriteLine(text);
            Console.WriteLine(email);
            await new EmailController().SendEmailAsync("info@ksm-agro.com", "Новый клиент - " + name, text +" "+ phone);
            await new EmailController().SendEmailAsync(email, "Спасибо за обращение - " + name, text);
            return Content("{content: succes}");
        }
    }
}
