﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KSM_Agro.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KSM_Agro.Controllers
{
    public class DealInfoesController : Controller
    {
        private readonly DataBaseContext _context;

        public DealInfoesController(DataBaseContext context)
        {
            _context = context;
        }

        [HttpGet("/panel/deals")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.DealInfo.Include(d=>d.dealStatus).Include(d => d.department).Include(d => d.dealType).Include(d=>d.Product).ToListAsync());
        }

        [HttpGet("/panel/deals/{id?}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Deal[] subdeals = _context.Deals.Where(p => p.ParentDealID == id).OrderBy(p => p.ChildNumber).Include(d => d.Product).Include(d => d.Department).Include(d => d.DealType).ToArray();
            ViewBag.SubDeals = subdeals;
            if (subdeals.Length != 0)
            {
                DealInfo subDealsInfo = DealInfo.CalculateDealInfo(subdeals);
            }
            
            var dealInfo = await _context.DealInfo.Include(d=>d.dealStatus).Include(d => d.department).Include(d => d.dealType)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (dealInfo == null)
            {
                return NotFound();
            }
            ViewBag.User = _context.Users.Find(int.Parse( HttpContext.User.Identity.Name));
            return View(dealInfo);
        }

        [HttpGet("/panel/deals/new")]
        public IActionResult Create()
        {
            List<Product> ProductsDict = _context.Products.ToList();
            List<Department> DepartDict = _context.Departments.ToList();
            ViewBag.Products = new SelectList(ProductsDict, "ProductId", "ProductTitle");
            ViewBag.Companies = new SelectList(DepartDict, "DepartmentID", "DepartmentTitle");
            return View();
        }

        // POST: DealInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost("/panel/deals/new")]
        public async Task<IActionResult> Create(DealInfo deal, string Product, string Department, int ObjemTovar)
        {
            if (ModelState.IsValid)
            {
                //Deal newDeal = new Deal();
                deal.department = _context.Departments.Where(d => d.DepartmentID == int.Parse(Department)).FirstOrDefault();
                deal.Creator = _context.Users.Where(d => d.UserId == int.Parse(HttpContext.User.Identity.Name)).FirstOrDefault();
                deal.dealStatus = _context.DealStatus.Find(1);
                DateTime now = DateTime.Now;
                deal.ShipPriceWithNDSForOneKG = deal.ShipPriceWithNDSForOneKG;
                deal.SellPriceWithNDSForOneKG = deal.SellPriceWithNDSForOneKG;
                deal.BuyPriceWithNDSForOneKG = deal.BuyPriceWithNDSForOneKG;
                deal.DealDS = now.Date;
                deal.Product = _context.Products.Where(p => p.ProductId == int.Parse(Product)).FirstOrDefault();
                Console.WriteLine(JsonConvert.SerializeObject(deal.Product));
                deal.GUID = Guid.NewGuid().ToString();
                Console.WriteLine("Объем отгрузки: {0}", ObjemTovar);
                deal.Price = ObjemTovar;
                _context.Add(deal);
                await _context.SaveChangesAsync();
                int ID = _context.DealInfo.Where(d => d.GUID == deal.GUID).FirstOrDefault().ID;
                deal.UID = "SQ" + ID.ToString("D4");
                _context.Entry(deal).Property("UID").IsModified = true;
                _context.SaveChanges();
                await new EmailController().SendEmailAsync("techtrashh@gmail.com", "Новая сделка!!!", "<strong>Здравствуйте Юрий. Менеджер " + deal.Creator.Name + " " + deal.Creator.LastName + " создал новую сделку через конструктор. Просим Вас провести анализ по этой сделке и одобрить. Ссылка на сделку <a href='https://ksm-agro.com/panel/deals/'"+ID+">"+deal.UID+ "</a></strong>");
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        [HttpGet("/panel/deals/delete/{id?}")]
        public IActionResult Delete(int? id)
        {
            var dealInfo = _context.DealInfo.Find(id);
            return View(dealInfo);
        }

        // POST: DealInfoes/Delete/5
        [HttpPost("/panel/deals/delete/{id?}")]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var dealInfo = await _context.DealInfo.FindAsync(id);
            Deal[] deals = _context.Deals.Where(d => d.ParentDealID == id).ToArray();
            _context.DealInfo.Remove(dealInfo);
            _context.Deals.RemoveRange(deals);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        [HttpPost("/panel/deals/submit/{id?}")]
        public IActionResult SubmitDeal(int? id)
        {
            DealInfo dealInfo = _context.DealInfo.Include(d=>d.dealStatus).Where(d=>d.ID==id).FirstOrDefault();
            if (dealInfo != null && dealInfo.dealStatus.ID==1)
            {
                DealStatus deal1 = _context.DealStatus.Where(d => d.ID == 2).FirstOrDefault();
                dealInfo.dealStatus = deal1;
                _context.DealInfo.Update(dealInfo);
                _context.SaveChanges();
                return Content(new JObject(new JProperty("submit", true)).ToString());
            }
            return Content(new JObject(new JProperty("submit", false)).ToString());
        }


        private bool DealInfoExists(int id)
        {
            return _context.DealInfo.Any(e => e.ID == id);
        }
    }
}
