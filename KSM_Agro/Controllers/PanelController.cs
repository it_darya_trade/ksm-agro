﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSM_Agro.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KSM_Agro.Controllers
{
    [Authorize(Roles = "4,3,2,1")]
    public class PanelController : Controller
    {
        private DataBaseContext _context;
        private readonly IHostingEnvironment _appEnvironment;
        public PanelController(DataBaseContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        [HttpGet("/panel")]
        public IActionResult Index()
        {
            ViewData["User"] = HttpContext.Session.GetString("User");
            if (HttpContext.User.IsInRole("1"))
            {
                return View();
            }
            else
            {
                return View();
            }
        }


        [HttpGet("/panel/user/myprofile")]
        [Authorize(Roles = "4,3,2,1")]
        public async Task<IActionResult> UserPage() {
            User user = _context.Users.Where(u => u.UserId == int.Parse(HttpContext.User.Identity.Name)).FirstOrDefault();
            return View();
        }
    }

    
}