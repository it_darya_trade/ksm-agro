﻿using System.IO;
using Microsoft.AspNetCore.Http;

namespace KSM_Agro.Controllers
{
    public class FileUploadController
    {
        public string ImageUploader(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return "error";
            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/images/uploaded",
                        file.FileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            var newImagePath = "/images/uploaded/" + file.FileName;

            return newImagePath;
        }

        public string FileUploader(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return "error";
            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/files/uploaded",
                        file.FileName);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            var newImagePath = "/files/uploaded/" + file.FileName;

            return newImagePath;
        }
    }
}