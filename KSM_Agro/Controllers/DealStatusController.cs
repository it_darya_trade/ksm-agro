﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KSM_Agro.Models;

namespace KSM_Agro.Controllers
{
    public class DealStatusController : Controller
    {
        private readonly DataBaseContext _context;

        public DealStatusController(DataBaseContext context)
        {
            _context = context;
        }

        // GET: DealStatus
        public async Task<IActionResult> Index()
        {
            return View(await _context.DealStatus.ToListAsync());
        }

        // GET: DealStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dealStatus = await _context.DealStatus
                .FirstOrDefaultAsync(m => m.ID == id);
            if (dealStatus == null)
            {
                return NotFound();
            }

            return View(dealStatus);
        }

        // GET: DealStatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DealStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title")] DealStatus dealStatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(dealStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(dealStatus);
        }

        // GET: DealStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dealStatus = await _context.DealStatus.FindAsync(id);
            if (dealStatus == null)
            {
                return NotFound();
            }
            return View(dealStatus);
        }

        // POST: DealStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title")] DealStatus dealStatus)
        {
            if (id != dealStatus.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dealStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DealStatusExists(dealStatus.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(dealStatus);
        }

        // GET: DealStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dealStatus = await _context.DealStatus
                .FirstOrDefaultAsync(m => m.ID == id);
            if (dealStatus == null)
            {
                return NotFound();
            }

            return View(dealStatus);
        }

        // POST: DealStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dealStatus = await _context.DealStatus.FindAsync(id);
            _context.DealStatus.Remove(dealStatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DealStatusExists(int id)
        {
            return _context.DealStatus.Any(e => e.ID == id);
        }
    }
}
