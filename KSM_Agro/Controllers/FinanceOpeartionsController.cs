﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KSM_Agro.Models;

namespace KSM_Agro.Controllers
{
    public class FinanceOpeartionsController : Controller
    {
        private readonly DataBaseContext _context;

        public FinanceOpeartionsController(DataBaseContext context)
        {
            _context = context;
        }

        // GET: FinanceOpeartions
        public async Task<IActionResult> Index()
        {
            return View(await _context.FinanceOpeartion.OrderBy(f=>f.ID).Include(f=>f.cash).Include(f => f.contrAgent).Include(f => f.department).ToListAsync());
        }

        // GET: FinanceOpeartions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var financeOpeartion = await _context.FinanceOpeartion
                .FirstOrDefaultAsync(m => m.ID == id);
            if (financeOpeartion == null)
            {
                return NotFound();
            }

            return View(financeOpeartion);
        }

        // GET: FinanceOpeartions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FinanceOpeartions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,operationType,ActBU,ActDate,SummWithNDS,NDS,SummWithOutNDS,Description")] FinanceOpeartion financeOpeartion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(financeOpeartion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(financeOpeartion);
        }

        [HttpGet("/reestr/page/{page?}")]
        public IActionResult Paginate(int? page) {
            int first = _context.FinanceOpeartion.OrderBy(f => f.ID).First().ID;
            return View("Views/FinanceOpeartions/Index.cshtml", _context.FinanceOpeartion.Where(f => f.ID >= first));
        }


        // GET: FinanceOpeartions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var financeOpeartion = await _context.FinanceOpeartion.FindAsync(id);
            if (financeOpeartion == null)
            {
                return NotFound();
            }
            return View(financeOpeartion);
        }

        // POST: FinanceOpeartions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,operationType,ActBU,ActDate,SummWithNDS,NDS,SummWithOutNDS,Description")] FinanceOpeartion financeOpeartion)
        {
            if (id != financeOpeartion.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(financeOpeartion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FinanceOpeartionExists(financeOpeartion.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(financeOpeartion);
        }

        // GET: FinanceOpeartions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var financeOpeartion = await _context.FinanceOpeartion
                .FirstOrDefaultAsync(m => m.ID == id);
            if (financeOpeartion == null)
            {
                return NotFound();
            }

            return View(financeOpeartion);
        }

        // POST: FinanceOpeartions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var financeOpeartion = await _context.FinanceOpeartion.FindAsync(id);
            _context.FinanceOpeartion.Remove(financeOpeartion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FinanceOpeartionExists(int id)
        {
            return _context.FinanceOpeartion.Any(e => e.ID == id);
        }
    }
}
