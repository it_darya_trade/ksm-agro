﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KSM_Agro.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace KSM_Agro.Controllers.Api
{
    [ApiController]
    public class ContrAgentsController : ControllerBase
    {
        private readonly DataBaseContext _context;
        public ContrAgentsController(DataBaseContext context) {
            _context = context;
        }
        // GET: api/ContrAgents
        [HttpGet("/api/contragents")]
        public IActionResult Get()
        {
            List<ContrAgent> contras = _context.ContrAgents.ToList();
            string contr_s = JsonConvert.SerializeObject(contras);
            return Content(contr_s);
        }

        [HttpPost("api/contragents/find/{query?}")]
        public IActionResult Find(string query) {
            Console.WriteLine("Query is: {0}", query);
            List<ContrAgent> contrAgents = _context.ContrAgents.Where(c=>c.ContrAgent_Name.ToLower().Contains(query)).ToList();
            return Content(JsonConvert.SerializeObject(contrAgents));
        }
    }
}
