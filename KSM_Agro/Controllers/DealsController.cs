﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using KSM_Agro.Models;
using Microsoft.AspNetCore.Authorization;
using Nancy.Json;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace KSM_Agro.Controllers
{
    [Authorize(Roles = "4,3,2,1")]
    public class DealsController : Controller
    {
        private readonly DataBaseContext _context;

        public DealsController(DataBaseContext context)
        {
            _context = context;
        }
        [HttpGet("/panel/deals/{parent?}/otgruzki/{id?}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var deal = await _context.Deals
                .FirstOrDefaultAsync(m => m.DealID == id);
            if (deal == null)
            {
                return NotFound();
            }
            Deal[] subdeals = _context.Deals.Where(p => p.ParentDealID == id).OrderBy(p => p.ChildNumber).Include(d => d.Product).Include(d => d.Department).Include(d => d.DealType).ToArray();
            ViewBag.SubDeals = subdeals;
            return View(deal);
        }

        // GET: Deals/Create
        //[HttpGet("/panel/deals/create")]
        //public IActionResult Create()
        //{
        //    
        //    
        //    
        //    
        //    return View();
        //}

        // POST: Deals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost("/panel/deals/create"), ActionName("Create")]
        public async Task<IActionResult> Create(Deal deal, string Product, string Department)
        {
            if (ModelState.IsValid)
            {
                //Deal newDeal = new Deal();
                deal.Department = _context.Departments.Where(d => d.DepartmentID == int.Parse(Department)).FirstOrDefault();
                deal.DealCreator = _context.Users.Where(d => d.UserId == int.Parse(HttpContext.User.Identity.Name)).FirstOrDefault();
                DateTime now = DateTime.Now;
                deal.ShipPriceWithNDSForOneKG=deal.ShipPriceWithNDSForOneKG;
                deal.SellPriceWithNDSForOneKG = deal.SellPriceWithNDSForOneKG;
                deal.BuyPriceWithNDSForOneKG = deal.BuyPriceWithNDSForOneKG;
                deal.DealDS = now.Date;
                deal.Product = _context.Products.Where(p => p.ProductId == int.Parse(Product)).FirstOrDefault();
                deal.GUID = Guid.NewGuid().ToString();
                _context.Add(deal);
                await _context.SaveChangesAsync();
                int ID = _context.Deals.Where(d => d.GUID == deal.GUID).FirstOrDefault().DealID;
                deal.DealUID = "SQ" + ID.ToString("D4");
                _context.Entry(deal).Property("DealUID").IsModified=true;
                _context.SaveChanges();
                return RedirectPermanent("/panel/deals");
            }
            return View(deal);
        }

        // GET: Deals/Edit/5
        //[HttpGet("/panel/deals/edit/{id?}")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deal = await _context.Deals.FindAsync(id);
            if (deal == null)
            {
                return NotFound();
            }
            List<Product> ProductsDict = _context.Products.ToList();
            List<Department> DepartDict = _context.Departments.ToList();
            List<DealType> TypeDict = _context.DealTypes.ToList();
            List<ContrAgent> Contragents = _context.ContrAgents.ToList();
            Console.WriteLine(_context.DealTypes.ToList().ToString());
            ViewBag.Products = ProductsDict;
            ViewBag.Companies = new SelectList(DepartDict, "DepartmentID", "DepartmentTitle");
            ViewBag.DealType = new SelectList(TypeDict, "Id", "Title");
            ViewBag.Contragents = new SelectList(Contragents, "ContrAgentID", "ContrAgent_Name");
            return View(deal);
        }

        // POST: Deals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost("/panel/deals/edit/{id?}")]
        public async Task<IActionResult> Edit(int? id, [Bind("DealID,ParentDealID,ChildNumber,DealType,DealDS,ShipAddress,Shippername,Carname,Rasstoyanie,Price,ShipPrice,DonePrice,SellPriceNDS,BuyPriceNDS,ShipPriceNDS,SafePriceNDS,MoneyNDS,DeltaShipping,DeltaProshenaya,SummDeltaShipp,DeltaCompinsation,DeltaPrice,SubmitDelta,transoprtScheme,ShipPriceWithNDS,ShipSancForNum,TransportdopRaschod,transoprtSummWithNDS,transoprtShipNDS,transoprtShipWithOutNDS,ACTBUNumber,ACTBUDate,TTNType,transoprtPayWithNDS,buyScheme,buyWithNDS,buySancForLost,buyWithCompensation,withOutNDS,buyNDS,buyActBU,buyActDate,buySummWithNDS,cSNumber,cSShipPoint,cSOtgruz,cSOneKGPriceWithoutNDS,cSSumm,safePriceWithNDS,safeNDSSUMM,safeSUMMWithOutNDS,safeActBU,safeActDate,raschAgentskije,raschProchee,raschSummAll,prodWithNDS,prodWithOutNDS,prodNDSSumm,prodDogovor,prodActBU,prodActDate,prodPayWithNDS,nakRaschWithOutNDS,nakRaschWithOurRaspred,nakRasch,nakRaschSUMM,marjPribyl,marjiNalnost,marjaOneKg,prybDoNP,nalogNP,chistPryb,chistPrybOneKG,chistPrybRenta,poteriPriDostWithOutCompensation,poteriPribPriDostWithOutCompensation,poteriDostavPrijem,poteriPryb,chistPrybnedPryb,chistPrybOneKGFinish,finishRenta")] Deal deal)
        {
            Console.WriteLine("ID:{0}; DealID:{1}", id, deal.DealID);
            if (id != deal.DealID)
            {
                
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(deal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DealExists(deal.DealID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectPermanent("/panel/deals");
            }
            List<Product> ProductsDict = _context.Products.ToList();
            List<Department> DepartDict = _context.Departments.ToList();
            List<DealType> TypeDict = _context.DealTypes.ToList();
            List<ContrAgent> Contragents = _context.ContrAgents.ToList();
            Console.WriteLine(_context.DealTypes.ToList().ToString());
            ViewBag.Products = ProductsDict;
            ViewBag.Companies = new SelectList(DepartDict, "DepartmentID", "DepartmentTitle");
            ViewBag.DealType = new SelectList(TypeDict, "Id", "Title");
            ViewBag.Contragents = new SelectList(Contragents, "ContrAgentID", "ContrAgent_Name");
            return View(deal);
        }

        // GET: Deals/Delete/5
        //[HttpGet("/panel/deals/delete/{id?}")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deal = await _context.Deals
                .FirstOrDefaultAsync(m => m.DealID == id);
            if (deal == null)
            {
                return NotFound();
            }

            return View(deal);
        }

        // POST: Deals/Delete/5
        //[HttpPost("/panel/deals/delete/{id?}"), ActionName("Delete")]
       // [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deal = await _context.Deals.FindAsync(id);
            _context.Deals.Remove(deal);
            await _context.SaveChangesAsync();
            return RedirectPermanent("/panel/deals");
        }

        private bool DealExists(int id)
        {
            return _context.Deals.Any(e => e.DealID == id);
        }


        [HttpPost("/panel/deals/new/calculate")]
        public IActionResult Calculate(int Product, string Department, float BuyPriceWithNDSForOneKG, float ShipPriceWithNDSForOneKG, float SellPriceWithNDSForOneKG, int PoteriDostavkaH4, int PoteriPrijemH12, float dopraschsumm, int CheckNakRash, float ObjemTovar, float buyNDS, float ShipPriceNDS, float SellPriceNDS, float PrijemCompenstionH14, float DostavCompenstionH6, float MoneyNDS) {
            float PokazatelPoterPurple = 1.002631344f;
            float PokazatelPoterGreen = 0.99126302892f;
            float BuyWithNDS = ObjemTovar * BuyPriceWithNDSForOneKG;
            float BuyWithOutNDS = CalculateWithOutNDS(BuyWithNDS, buyNDS);
            float StatPoteriForCalculatePurple = 0;
            if (PokazatelPoterPurple > 1) {
                StatPoteriForCalculatePurple = 0;
                
            } else {
                StatPoteriForCalculatePurple = ObjemTovar - PokazatelPoterPurple * ObjemTovar;
            }
            float StatPoteriForCalculateGreen = 0;
            if (PokazatelPoterGreen > 1){
                StatPoteriForCalculateGreen = 0;
            }
            else{
                StatPoteriForCalculateGreen = ObjemTovar - PokazatelPoterGreen * ObjemTovar;
            }
            float ShipWithNDS = ShipPriceWithNDSForOneKG * ObjemTovar;
            float PoteriPriDostavke = 0;
            if ((int)PoteriDostavkaH4 == 1 && (int)DostavCompenstionH6 == 2)
            {
                PoteriPriDostavke = StatPoteriForCalculatePurple;
            }
            float DostavkaWithPoteri = ShipWithNDS;
            if (PoteriPriDostavke > 0)
            {
                DostavkaWithPoteri = (ObjemTovar - PoteriPriDostavke) * ShipPriceWithNDSForOneKG;
            }
            else
            {
                DostavkaWithPoteri = ShipWithNDS;
            }

            float ShipPriceWithOutNDS = CalculateWithOutNDS(DostavkaWithPoteri, ShipPriceNDS);
            float PoteriWhenPrijemkaBezCompen = 0;
            if ((int)PoteriPrijemH12 == 1) {
                PoteriWhenPrijemkaBezCompen = StatPoteriForCalculateGreen;
            }
            else
            {
                PoteriWhenPrijemkaBezCompen = 0;
            }
            float PoteriWhenDostavkaBezCompen = 0;
            if ((int)PoteriDostavkaH4 == 1 && (int)DostavCompenstionH6 == 0)
            {
                PoteriWhenDostavkaBezCompen = StatPoteriForCalculatePurple;
            }
            else {
                PoteriWhenDostavkaBezCompen = 0;
            }
            float Shtrafnije = 0;
            if (((int)PoteriDostavkaH4 == 1 && (int)DostavCompenstionH6 == 0) && ((int)PoteriPrijemH12 == 1 && (int)PrijemCompenstionH14 == 0))
            {
                Shtrafnije = StatPoteriForCalculateGreen + StatPoteriForCalculatePurple;
            }
            else if ((int)PoteriDostavkaH4 == 1 && (int)DostavCompenstionH6 == 0)
            {
                Shtrafnije = StatPoteriForCalculatePurple;
            }
            else if ((int)PoteriPrijemH12 == 1 && (int)PrijemCompenstionH14 == 0) {
                Shtrafnije = StatPoteriForCalculateGreen;
            }
            
            float ShtrafnijeSumma = Shtrafnije * SellPriceWithNDSForOneKG;
            float StrafnijeBezNDS = CalculateWithOutNDS(ShtrafnijeSumma, SellPriceNDS);
            float UpuschPribDoNaloga = StrafnijeBezNDS - Shtrafnije * CalculateWithOutNDS(BuyPriceWithNDSForOneKG, buyNDS) - Shtrafnije * CalculateWithOutNDS(ShipPriceWithNDSForOneKG, ShipPriceNDS);
            float UouschPribylAfterNalog = UpuschPribDoNaloga;
            if ((int)MoneyNDS == 1)
            {
                UouschPribylAfterNalog = UpuschPribDoNaloga - StrafnijeBezNDS * (MoneyNDS / 100);
            }
            else
            {
                UouschPribylAfterNalog = UpuschPribDoNaloga - UpuschPribDoNaloga * (MoneyNDS / 100);
            }


            float WasSelled = ObjemTovar - PoteriWhenDostavkaBezCompen - PoteriWhenPrijemkaBezCompen;
            float SellsWithNDS = WasSelled * SellPriceWithNDSForOneKG;
            if ((int)PoteriPrijemH12 == 1 && (int)PrijemCompenstionH14 == 1)
            {
                SellsWithNDS = WasSelled * SellPriceWithNDSForOneKG + PoteriWhenPrijemkaBezCompen * SellPriceWithNDSForOneKG;
            }
            else {
                SellsWithNDS = WasSelled * SellPriceWithNDSForOneKG;
            }

            float SellsBezNDS = CalculateWithOutNDS(SellsWithNDS, SellPriceNDS);
            float PrybylDonaloga = SellsBezNDS - BuyWithOutNDS - ShipPriceWithOutNDS - dopraschsumm;
            float Prybal = PrybylDonaloga;
            if (MoneyNDS == 1)
            {
                Prybal = PrybylDonaloga - SellsBezNDS * (MoneyNDS / 100);
            }
            else
            {
                Prybal = PrybylDonaloga - PrybylDonaloga * (MoneyNDS / 100);
            }

            float PrybylItogo = Prybal - UouschPribylAfterNalog;
            Console.WriteLine("Штрафные объемы: " + SellsBezNDS);
            float Vyruchka = SellsBezNDS - StrafnijeBezNDS;
            float dolyaNakladnyh = 1.68f;
            float pryamZatraty = BuyWithOutNDS+ShipPriceWithOutNDS+dopraschsumm+ Shtrafnije * CalculateWithOutNDS(BuyPriceWithNDSForOneKG, buyNDS) + Shtrafnije * CalculateWithOutNDS(ShipPriceWithNDSForOneKG, ShipPriceNDS);
            float SummaNakladnyh = pryamZatraty *dolyaNakladnyh/ (100 - dolyaNakladnyh);
            float ChistPrybDoNalog = PrybylDonaloga + UpuschPribDoNaloga - SummaNakladnyh;
            float ChistPribyl = ChistPrybDoNalog;
            if (MoneyNDS == 1)
            {
                ChistPribyl = ChistPrybDoNalog - SellsBezNDS * (MoneyNDS / 100);
            }
            else
            {
                ChistPribyl = ChistPrybDoNalog - ChistPrybDoNalog * (MoneyNDS / 100);
            }

            float MarjaForOneKG = PrybylItogo/ObjemTovar;
            float ChistForOneKG = ChistPribyl/ObjemTovar;
            Dictionary<string, float> result = new Dictionary<string, float> {
                {"vyruchka", Vyruchka },
                {"marja", PrybylItogo },
                { "chist", ChistPribyl} ,
                {"marjakg", MarjaForOneKG },
                {"chistkg", ChistForOneKG }
            };

            var d = result.ToDictionary(item => item.Key.ToString(), item => item.Value.ToString());
            var json = new JavaScriptSerializer().Serialize(d);
            return Content(json);
        }

        public float CalculateWithOutNDS(float price, float nds) {
            if (nds == 0)
            {
                return price;
            }
            else {
                return price / (1 + nds / 100);
            }
        }
        [HttpGet("/panel/deals/{parentId?}/otgruzki/new")]
        public IActionResult NewEtap(int? parentId) {
            List<Product> ProductsDict = _context.Products.ToList();
            List<Department> DepartDict = _context.Departments.ToList();
            List<DealType> TypeDict = _context.DealTypes.ToList();
            List<ContrAgent> Contragents = _context.ContrAgents.ToList();
            Console.WriteLine(_context.DealTypes.ToList().ToString());
            ViewBag.ParentDeal = parentId;
            ViewBag.Products = ProductsDict;
            ViewBag.Companies = new SelectList(DepartDict, "DepartmentID", "DepartmentTitle");
            ViewBag.DealType = new SelectList(TypeDict, "Id", "Title");
            ViewBag.Contragents = new SelectList(Contragents, "ContrAgentID", "ContrAgent_Name");
            HttpContext.Session.SetString("ParentDealID", parentId.ToString());
            ViewBag.ParentDealID = parentId;
            return View();
        }


        [HttpPost("/panel/deals/{parent?}/otgruzki/new")]
        public async Task<IActionResult> NewEtap(Deal deal, int? parentId, string Product, string DealType, string SelledId)
        {
            if (deal is null)
            {
                throw new ArgumentNullException(nameof(deal));
            }
            if (ModelState.IsValid)
            {
                Deal dparent = _context.Deals.Where(p=>p.DealID==parentId).Include(p=>p.Department).FirstOrDefault();
                Department depparent = _context.Departments.Find(dparent.Department.DepartmentID);
                deal.DealType = _context.DealTypes.Find(int.Parse(DealType));
                deal.ParentDealID = dparent.DealID;
                deal.Product = _context.Products.Find(int.Parse(Product));
                deal.Department = _context.Departments.Find(depparent.DepartmentID);
                deal.ChildNumber = _context.Deals.Where(p => p.ParentDealID == parentId).Count()+1;
                deal.DealUID = _context.Deals.Find(parentId).DealUID + "-" + deal.ChildNumber.ToString();
                deal.GUID = Guid.NewGuid().ToString();
                deal=CalculateDeal(deal);
                _context.Add(deal);
                await _context.SaveChangesAsync();
                return RedirectPermanent("/panel/deals");
            }
            List<Product> ProductsDict = _context.Products.ToList();
            List<Department> DepartDict = _context.Departments.ToList();
            List<DealType> TypeDict = _context.DealTypes.ToList();
            List<ContrAgent> Contragents = _context.ContrAgents.ToList();
            Console.WriteLine(_context.DealTypes.ToList().ToString());
            ViewBag.Products = ProductsDict;
            ViewBag.Companies = new SelectList(DepartDict, "DepartmentID", "DepartmentTitle");
            ViewBag.DealType = new SelectList(TypeDict, "Id", "Title");
            ViewBag.Contragents = new SelectList(Contragents, "ContrAgentID", "ContrAgent_Name");
            HttpContext.Session.SetString("ParentDealID", parentId.ToString());
            ViewBag.ParentDealID = parentId;
            return View(deal);
        }

        [HttpPost("/panel/deals/calculateetap/{parentId?}")]
        public IActionResult NewEtap(Deal deal, string DealType, string Product,string ShipperId, string SelledId, int DepartmentID, int? parentId)
        {
            Console.WriteLine("ID РОДМИЕЛЬСКОЙ СДЕЛКИ {0}", parentId);
            DealInfo dparent = _context.DealInfo.Where(p => p.ID == parentId).Include(p => p.department).FirstOrDefault();
            Console.WriteLine(JsonConvert.SerializeObject(dparent));
            Department depparent = _context.Departments.Find(dparent.department.DepartmentID);
            deal.ParentDealID = dparent.ID;
            deal.Department = _context.Departments.Find(depparent.DepartmentID);
            deal.SelledId = _context.ContrAgents.Where(c => c.ContrAgentID == int.Parse(SelledId)).FirstOrDefault();// Find(int.Parse(SelledId));
            deal.ShipperId = _context.ContrAgents.Where(c=>c.ContrAgentID== int.Parse(ShipperId)).FirstOrDefault();
            deal.DealType = _context.DealTypes.Find(int.Parse(DealType));
            deal.Product = _context.Products.Find(int.Parse(Product));
            deal = CalculateDeal(deal);
            return Content(JsonConvert.SerializeObject(deal).ToString());
        }



        public Deal CalculateDeal(Deal deal)
        {
            deal.CarStatus = "принято";
            deal.DeltaCompinsation = 4;
            deal.Season = SetDealSeason();
            if (deal.DealType.Id == 3)
            {
                deal.DeltaShipping = 0;
            }
            else { deal.DeltaShipping = deal.Price - deal.ShipPrice; }
            if (deal.DeltaShipping < 0)
            {
                deal.SummDeltaShipp = 0;
            }
            else
            {
                deal.SummDeltaShipp = deal.DeltaShipping - deal.DeltaProshenaya;
            }
            if (deal.DealType.Id == 2)
            {
                deal.SubmitDelta = 0;
            }
            else
            {
                deal.DeltaShipping = deal.ShipPrice - deal.DonePrice;
            }
            if (deal.transoprtScheme == TransoprtScheme.ByShipCount)
            {
                deal.ShipPriceWithNDS = deal.ShipPrice * deal.ShipPriceWithNDSForOneKG;
            }
            else
            {
                deal.ShipPriceWithNDS = deal.Price * deal.ShipPriceWithNDSForOneKG;
            }

            if (deal.DeltaCompinsation == 0)
            {
                deal.ShipSancForNum = deal.SummDeltaShipp * deal.DeltaPrice;
            }
            else
            {
                deal.ShipSancForNum = 0;
            }
            deal.transoprtSummWithNDS = (deal.ShipPriceWithNDS - deal.ShipSancForNum + deal.TransportdopRaschod) / (deal.ShipPriceNDS / 100 + 1);
            deal.transoprtShipWithOutNDS = deal.transoprtSummWithNDS / (deal.ShipPriceNDS / 100 + 1);
            deal.transoprtShipNDS = deal.transoprtSummWithNDS - deal.transoprtShipWithOutNDS;

            if (deal.buyScheme == TransoprtScheme.ByShipCount)
            {
                deal.buyWithNDS = deal.ShipPrice * deal.BuyPriceWithNDSForOneKG;
            }
            else { deal.buyWithNDS = deal.Price * deal.BuyPriceWithNDSForOneKG; }
            if (deal.DeltaCompinsation == 1)
            {
                deal.buySancForLost = deal.SummDeltaShipp * deal.DeltaPrice;
            }
            else { deal.buySancForLost = 0; }
            deal.buyWithCompensation = deal.buyWithNDS - deal.buySancForLost;
            deal.buyNDS = deal.buyWithCompensation - deal.buyNDS;
            deal.withOutNDS =  deal.buyWithCompensation / (deal.BuyPriceNDS + 1);
            deal.cSSumm = deal.cSOtgruz * deal.cSOneKGPriceWithoutNDS;
            deal.safePriceWithNDS = deal.DonePrice * deal.SafePriceForOneKG;
            deal.safeSUMMWithOutNDS = CalculateWithNDS(deal.safePriceWithNDS, deal.SafePriceNDS);
            deal.safeNDSSUMM = deal.safePriceWithNDS - deal.safeSUMMWithOutNDS;
            deal.raschSummAll = deal.safeSUMMWithOutNDS + deal.raschAgentskije + deal.raschProchee + deal.cSSumm;
            deal.prodWithNDS = deal.DonePrice * deal.SellPriceWithNDSForOneKG;
            deal.prodWithOutNDS = CalculateWithNDS(deal.prodWithNDS, deal.SellPriceNDS);
            deal.prodNDSSumm = deal.prodWithNDS - deal.prodWithOutNDS;
            deal.nakRaschWithOutNDS = deal.transoprtShipWithOutNDS + deal.withOutNDS + deal.raschSummAll;
            deal.bazaRaspredByDepartment = deal.nakRaschWithOutNDS / (_context.Deals.Where(f => f.Department.DepartmentID == deal.Department.DepartmentID).Where(f => f.Season == deal.Season).Where(f => f.DealDS.Year == deal.DealDS.Year).Where(f => f.DealDS.Month == deal.DealDS.Month).Sum(p => p.nakRasch));
            deal.nakRaschOfDepartmentWithOutNP = (_context.FinanceOpeartion.Where(f => f.operationType == Operation.Reailise).Where(f => f.statya.PLtype == "НАКЛАДНЫЕ РАСХОДЫ").Where(f => f.department.DepartmentID == deal.Department.DepartmentID).Where(f => f.Season == deal.Season).Sum(f => f.SummWithNDS) -
            _context.FinanceOpeartion.Where(f => f.operationType == Operation.Reailise).Where(f => f.statya.PLtype == "Прочие доходы").Where(f => f.department.DepartmentID == deal.Department.DepartmentID).Where(f => f.Season == deal.Season).Sum(f => f.SummWithNDS) -
            _context.FinanceOpeartion.Where(f => f.operationType == Operation.Reailise).Where(f => f.statya.Title == "Налог на прибыль").Where(f => f.department.DepartmentID == deal.Department.DepartmentID).Where(f => f.Season == deal.Season).Sum(f => f.SummWithNDS)) * deal.bazaRaspredByDepartment;
            deal.bazaRaspredelenijaAllDeals = deal.nakRaschWithOutNDS / (_context.Deals.Where(d => d.CarStatus == "принято").Where(d => d.Season == deal.Season).Sum(d => d.nakRaschOfDepartmentWithOutNP));
            deal.UKZatraty = _context.FinanceOpeartion.Where(f => f.statya.Title == "Затраты УК").Where(d=>d.Season==deal.Season).Where(f => f.statya.PLtype == "НАКЛАДНЫЕ РАСХОДЫ").Sum(d=>d.SummWithOutNDS);//..Sum(f => f.SummWithOutNDS) - _context.FinanceOpeartion.Where(f => f.department.DepartmentTitle == "УК").Where(f => f.statya.Title == "Прочие доходы").Where(f => f.Season == deal.Season).Sum(f => f.SummWithOutNDS)) * deal.bazaRaspredelenijaAllDeals;
            deal.nakRasch = deal.nakRaschOfDepartmentWithOutNP + deal.UKZatraty;
            deal.nakRaschSUMM = deal.nakRaschWithOutNDS + deal.nakRasch;
            deal.prybDoNP = deal.prodWithOutNDS - deal.nakRaschSUMM;
            if (deal.MoneyNDS != 0.01)
            {
                deal.nalogNP = CalculateWithNDS(deal.prybDoNP, deal.MoneyNDS);
            }
            else
            {
                deal.nalogNP = CalculateWithNDS(deal.prodWithOutNDS, deal.MoneyNDS);
            }
            deal.chistPryb = deal.prybDoNP - deal.nalogNP;
            deal.chistPrybOneKG = deal.chistPryb / deal.ShipPrice;
            deal.chistPrybRenta = deal.chistPryb / deal.prodWithOutNDS;
            if (deal.CarStatus == "принято")
            {
                deal.marjPribyl = deal.prodWithOutNDS - deal.nakRaschWithOutNDS;
                deal.marjiNalnost = deal.marjPribyl / deal.prodWithOutNDS;
                deal.marjaOneKg = deal.marjPribyl / deal.Price;
                deal.MPMinusNDSNP = deal.prodWithOutNDS - deal.nakRaschWithOutNDS - deal.nalogNP;
                deal.marjiNalnostNP = deal.MPMinusNDSNP / deal.prodWithOutNDS;
                deal.marjaOneKgNP = deal.MPMinusNDSNP / deal.ShipPrice;
            }
            else
            {
                deal.marjPribyl = 0;
                deal.marjiNalnost = 0;
                deal.marjaOneKg = 0;
                deal.MPMinusNDSNP = 0;
                deal.marjiNalnostNP = 0;
                deal.marjaOneKgNP = 0;
            }
            deal.poteriPriDostWithOutCompensation = 0;
            if (deal.CarStatus == "принято"&&deal.DeltaCompinsation==4&&deal.DeltaShipping>0) {
                deal.poteriPriDostWithOutCompensation = (deal.BuyPriceWithNDSForOneKG * deal.DeltaShipping) / (deal.BuyPriceNDS + 1) + (deal.DeltaShipping * deal.ShipPriceWithNDSForOneKG) / (deal.ShipPriceNDS + 1);
            }
            deal.poteriPribPriDostWithOutCompensation = 0;
            if(deal.CarStatus == "принято" && deal.DeltaShipping > 0 && deal.MoneyNDS == 0.01)
            {
                deal.poteriPribPriDostWithOutCompensation =
                    (deal.DeltaShipping * deal.SellPriceWithNDSForOneKG) / (1 + deal.SellPriceNDS) -
                    (deal.DeltaShipping * deal.BuyPriceWithNDSForOneKG) / (deal.BuyPriceNDS + 1) -
                    (deal.DeltaShipping * deal.ShipPriceWithNDSForOneKG) / (deal.ShipPriceNDS + 1) -
                    (deal.DeltaShipping * deal.BuyPriceWithNDSForOneKG) / (deal.BuyPriceNDS + 1)*deal.MoneyNDS;
            }
            if (deal.CarStatus == "принято" && deal.DeltaShipping > 0 && deal.MoneyNDS > 0.01) {
                deal.poteriPribPriDostWithOutCompensation =
                    (deal.DeltaShipping * deal.SellPriceWithNDSForOneKG) / (1 + deal.SellPriceNDS) -
                    (deal.DeltaShipping * deal.BuyPriceWithNDSForOneKG) / (deal.BuyPriceNDS + 1) -
                    (deal.DeltaShipping * deal.ShipPriceWithNDSForOneKG) / (deal.ShipPriceNDS + 1) -
                    ((deal.DeltaShipping * deal.SellPriceWithNDSForOneKG) / (1 + deal.SellPriceNDS) - 
                    (deal.DeltaShipping * deal.BuyPriceWithNDSForOneKG) / (deal.BuyPriceNDS + 1)-
                    (deal.DeltaShipping * deal.ShipPriceWithNDSForOneKG) / (deal.ShipPriceNDS + 1)) * deal.MoneyNDS;
            }
            deal.chistPrybnedPryb = deal.chistPryb - deal.poteriPriDostWithOutCompensation - deal.poteriDostavPrijem - deal.poteriPryb - deal.poteriPribPriDostWithOutCompensation;
            deal.chistPrybOneKGFinish = deal.chistPrybnedPryb / deal.Price;
            deal.finishRenta = deal.chistPrybnedPryb / deal.prodWithOutNDS;
            return deal;
        }

        float CalculateWithNDS(float value, float nds)
        {
            return value / (nds + 1);
        }

        string SetDealSeason() {
            string season="";
            DateTime date = DateTime.Now;
            if (date.Month < 6)
            {
                season = (date.Year - 1).ToString() + "-" + date.Year.ToString();
                return season;
            }
            else
            {
                season = date.Year.ToString() + "-" + (date.Year+1).ToString();
                return season;
            }
        }
    }
}
