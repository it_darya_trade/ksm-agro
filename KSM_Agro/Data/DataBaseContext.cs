﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KSM_Agro.Models;

namespace KSM_Agro.Models
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        {
        }

        public DbSet<Deal> Deals { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ContrAgent> ContrAgents { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<DealStatus> DealStatus { get; set; }
        public DbSet<DealType> DealTypes { get; set; }
        public DbSet<FinanceOpeartion> FinanceOpeartion { get; set; }
        public DbSet<Cash> Cash { get; set; }
        public DbSet<StatyaDvijeniya> StatyaDvijeniyas { get; set; }
        public DbSet<KSM_Agro.Models.DealInfo> DealInfo { get; set; }
    }
}
