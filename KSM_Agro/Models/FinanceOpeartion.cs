﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KSM_Agro.Models
{
    public enum Operation {
        [Display(Name="Оплата")]
        Pay=0,
        [Display(Name = "Реализация")]
        Reailise =1

    }
    public class FinanceOpeartion
    {
        public int ID { get; set; }
        [Display(Name ="Кошелек")]
        public Cash cash { get; set; }
        [Display(Name = "Подразделение")]
        public Department department { get; set; }
        [Display(Name = "Тип операции")]
        public Operation operationType { get; set; }
        [Display(Name = "Акт БУ")]
        public string ActBU { get; set; }
        [Display(Name = "Дата акта")]
        public DateTime ActDate { get; set; }
        [Display(Name = "Сумма с НДС")]
        public float SummWithNDS { get; set; }
        [Display(Name = "НДС")]
        public float NDS { get; set; }
        [Display(Name = "Сумма без НДС")]
        public float SummWithOutNDS { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Контрагент")]
        public ContrAgent contrAgent { get; set; }
        [Display(Name = "Статья движения")]
        public StatyaDvijeniya statya { get; set; }
        [Display(Name = "Товар")]
        public Product product { get; set; }
        [Display(Name = "Проверка БУ")]
        public bool CheckBU { get; set; }
        [Display(Name = "Задачи")]
        public string toDO { get; set; }
        public string Season { get; set; }
    }

    public class StatyaDvijeniya {
        public int ID { get; set;}
        [Display(Name = "Наименование")]
        public string Title { get; set; }
        [Display(Name = "Тип расходов PL")]
        public string PLtype { get; set; }
        [Display(Name = "Структура PL")]
        public string PLStruct { get; set; }
        [Display(Name = "Тип расходов PL")]
        public string CFStruct { get; set; }
        [Display(Name = "Тип расходов PL")]
        public string CFType { get; set; }
    }

    public class Cash {
        public int ID { get; set; }
        [Display(Name ="Наименование")]
        public string Title { get; set; }
        [Display(Name = "Средства")]
        public float Money { get; set; }
    }
}
