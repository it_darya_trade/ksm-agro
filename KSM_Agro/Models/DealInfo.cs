﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace KSM_Agro.Models
{
    public class DealInfo
    {
        [Key]
        public int ID { get; set; }
        public DealStatus dealStatus { get; set; }
        public string Season { get; set; }
        public User Creator { get; set; }
        public Product Product { get; set; }
        public DateTime DealDS { get; set; }
        public DealType dealType { get; set; }
        public Department department { get; set; }
        public string UID { get; set; }
        public int DealTimeLong { get; set; }
        public float Price { get; set; }
        public float DonePrice {get;set;}
        public float ShipPrice {get;set;}
        public float DeltaShipping {get;set;}
        public float SummDeltaShipp {get;set;}
        public float SubmitDelta {get;set;}
        public float TransportdopRaschod {get;set;}
        public float transoprtSummWithNDS {get;set;}
        public float transoprtShipNDS  {get;set;}
        public float transoprtShipWithOutNDS {get;set;}
        public float transoprtPayWithNDS {get;set;}
        public float buyWithNDS {get;set;}
        public float buySancForLost {get;set;}
        public float buyWithCompensation {get;set;}
        public float buyNDS {get;set;}
        public float withOutNDS  {get;set;}
        public float buySummWithNDS  {get;set;}
        public float cSOtgruz {get;set;}
        public float cSOneKGPriceWithoutNDS {get;set;}
        public float cSSumm {get;set;}
        public float safePriceWithNDS {get;set;}
        public float SafePriceNDS {get;set;}
        public float safeSUMMWithOutNDS {get;set;}
        public float raschAgentskije {get;set;}
        public float raschProchee {get;set;}
        public float raschSummAll {get;set;}
        public float prodWithNDS  {get;set;}
        public float prodNDSSumm {get;set;}
        public float prodWithOutNDS {get;set;}
        public float prodPayWithNDS {get;set;}
        public float nakRaschWithOutNDS {get;set;}
        public float nakRaschOfDepartmentWithOutNP {get;set;}
        public float nakRasch {get;set;}
        public float nakRaschSUMM {get;set;}
        public float marjPribyl {get;set;}
        public float marjiNalnost {get;set;}
        public float marjaOneKg {get;set;}
        public float MPMinusNDSNP {get;set;}
        public float marjiNalnostNP {get;set;}
        public float marjaOneKgNP {get;set;}
        public float prybDoNP {get;set;}
        public float nalogNP  {get;set;}
        public float chistPryb  {get;set;}
        public float chistPrybOneKG {get;set;}
        public float chistPrybRenta {get;set;}
        public float poteriPriDostWithOutCompensation {get;set;}
        public float poteriPribPriDostWithOutCompensation {get;set;}
        public float poteriPryb {get;set;}
        public float poteriDostavPrijem {get;set;}
        public float chistPrybnedPryb {get;set;}
        public float chistPrybOneKGFinish {get;set;}
        public float finishRenta {get;set;}
        public float BuyPriceWithNDSForOneKG {get;set;}
        public float ShipPriceWithNDSForOneKG {get;set;}
        public float SellPriceWithNDSForOneKG {get;set;}
        public float buyPriceNDS { get; set; }
        public float ShipPriceNDS { get; set; }
        public float SellPriceNDS { get; set; }
        public float MoneyNDS { get; set; }
        public string GUID { get; set; }


        public static DealInfo CalculateDealInfo(Deal[] deals)
        {
            DealInfo dealInfo = new DealInfo();
            dealInfo.Price = deals.Sum(d => d.Price);
            dealInfo.DonePrice = deals.Sum(d => d.DonePrice);
            dealInfo.ShipPrice = deals.Sum(d => d.ShipPrice);
            dealInfo.DeltaShipping = deals.Sum(d => d.DeltaShipping);
            dealInfo.SummDeltaShipp = deals.Sum(d => d.SummDeltaShipp);
            dealInfo.SubmitDelta = deals.Sum(d => d.SubmitDelta);
            dealInfo.TransportdopRaschod = deals.Sum(d => d.TransportdopRaschod);
            dealInfo.transoprtSummWithNDS = deals.Sum(d => d.transoprtSummWithNDS);
            dealInfo.transoprtShipNDS = deals.Sum(d => d.transoprtShipNDS);
            dealInfo.transoprtShipWithOutNDS = deals.Sum(d => d.transoprtShipWithOutNDS);
            dealInfo.transoprtPayWithNDS = deals.Sum(d => d.transoprtPayWithNDS);
            dealInfo.buyWithNDS = deals.Sum(d => d.buyWithNDS);
            dealInfo.buySancForLost = deals.Sum(d => d.buySancForLost);
            dealInfo.buyWithCompensation = deals.Sum(d => d.buyWithCompensation);
            dealInfo.buyNDS = deals.Sum(d => d.buyNDS);
            dealInfo.withOutNDS = deals.Sum(d => d.withOutNDS);
            dealInfo.buySummWithNDS = deals.Sum(d => d.ShipPriceWithNDS);
            dealInfo.cSOtgruz = deals.Sum(d => d.cSOtgruz);
            dealInfo.cSOneKGPriceWithoutNDS = deals.Average(d => d.cSOneKGPriceWithoutNDS);
            dealInfo.cSSumm = deals.Sum(d => d.cSSumm);
            dealInfo.safePriceWithNDS = deals.Sum(d => d.safePriceWithNDS);
            dealInfo.SafePriceNDS = deals.Sum(d => d.SafePriceNDS);
            dealInfo.safeSUMMWithOutNDS = deals.Sum(d => d.safeSUMMWithOutNDS);
            dealInfo.raschAgentskije = deals.Sum(d => d.raschAgentskije);
            dealInfo.raschProchee = deals.Sum(d => d.raschProchee);
            dealInfo.raschSummAll = deals.Sum(d => d.raschSummAll);
            dealInfo.prodWithNDS = deals.Sum(d => d.prodWithNDS);
            dealInfo.prodNDSSumm = deals.Sum(d => d.prodNDSSumm);
            dealInfo.prodWithOutNDS = deals.Sum(d => d.prodWithOutNDS);
            dealInfo.prodPayWithNDS = deals.Sum(d => d.prodPayWithNDS);
            dealInfo.nakRaschWithOutNDS = deals.Sum(d => d.nakRaschWithOutNDS);
            dealInfo.nakRaschOfDepartmentWithOutNP = deals.Sum(d => d.nakRaschOfDepartmentWithOutNP);
            dealInfo.nakRasch = deals.Sum(d => d.nakRasch);
            dealInfo.nakRaschSUMM = deals.Sum(d => d.nakRaschSUMM);
            dealInfo.marjPribyl = deals.Sum(d => d.marjPribyl);
            dealInfo.marjiNalnost = deals.Average(d => d.marjiNalnost) * 100;
            dealInfo.marjaOneKg = deals.Average(d => d.marjaOneKg);
            dealInfo.MPMinusNDSNP = deals.Sum(d => d.MPMinusNDSNP);
            dealInfo.marjiNalnostNP = deals.Average(d => d.marjiNalnostNP) * 100;
            dealInfo.marjaOneKgNP = deals.Average(d => d.marjaOneKgNP);
            dealInfo.prybDoNP = deals.Sum(d => d.prybDoNP);
            dealInfo.nalogNP = deals.Sum(d => d.nalogNP);
            dealInfo.chistPryb = deals.Sum(d => d.chistPryb);
            dealInfo.chistPrybOneKG = deals.Average(d => d.chistPrybOneKG);
            dealInfo.chistPrybRenta = (dealInfo.chistPryb/dealInfo.prodPayWithNDS) * 100;
            dealInfo.poteriPriDostWithOutCompensation = deals.Sum(d => d.poteriPriDostWithOutCompensation);
            dealInfo.poteriPribPriDostWithOutCompensation = deals.Sum(d => d.poteriPribPriDostWithOutCompensation);
            dealInfo.poteriPryb = deals.Sum(d => d.poteriPryb);
            dealInfo.poteriDostavPrijem = deals.Sum(d => d.poteriDostavPrijem);
            dealInfo.chistPrybnedPryb = deals.Sum(d => d.chistPrybnedPryb);
            dealInfo.chistPrybOneKGFinish = deals.Average(d => d.chistPrybOneKGFinish);
            dealInfo.finishRenta = deals.Average(d => d.finishRenta) * 100;
            dealInfo.BuyPriceWithNDSForOneKG = deals.Average(d => d.BuyPriceWithNDSForOneKG);
            dealInfo.ShipPriceWithNDSForOneKG = deals.Average(d => d.ShipPriceWithNDSForOneKG);
            dealInfo.SellPriceWithNDSForOneKG = deals.Average(d => d.SellPriceWithNDSForOneKG);
            return dealInfo;
        }
    }
}
