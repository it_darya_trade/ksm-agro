﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KSM_Agro.Models
{
    public class AdminController : Controller
    {
        [HttpGet("/admin")]
        [Authorize(Roles = "4")]
        public IActionResult Index()
        {
            return View();
        }
    }
}