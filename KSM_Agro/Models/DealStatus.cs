﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSM_Agro.Models
{
    public class DealStatus
    {
        public int ID { get; set; }
        public string Title{get;set;}
    }
}
