﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace KSM_Agro.Models
{
    public enum TransoprtScheme {
        /// <summary>
        /// по отгруженному количеству
        /// </summary>
        [DisplayName("по отгруженному количеству")]
        BySellCount =0,
        /// <summary>
        /// по доставленному количеству
        /// </summary>
        [DisplayName("по доставленному количеству")]
        ByShipCount =1
    }


    public enum Nds {
        [Display(Name ="0%")]
        Zero = 0,
        [Display(Name = "1%")]
        One = 1,
        [Display(Name = "10%")]
        Ten =10,
        [Display(Name = "18%")]
        Eighteen =18,
        [Display(Name = "20%")]
        Twenty =20}
    public class Deal
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DealID { get; set; }
        public int ParentDealID { get; set; }
        public int ChildNumber { get; set; }
        [DisplayName("Статус машины")]
        public string CarStatus { get; set; }
        public string Season { get; set; }
        public string DealUID { get; set; }
        public string GUID { get; set; }
        [DisplayName("Тип сделки")]
        public DealType DealType { get; set; }
        [DisplayName("Дата покупки")]
        public DateTime buyDate { get; set; }
        [DisplayName("Дата продажи")]
        public DateTime sellDate { get; set; }
        /// <summary>
        /// Время проведения сделки
        /// </summary>
        public DateTime DealDS { get; set; }
        /// <summary>
        /// Подразделение
        /// </summary>
        public Department Department { get; set; }
        public User DealCreator { get; set; }
        /// <summary>
        /// Товар
        /// </summary>
        [DisplayName("Товар")]
        public Product Product { get; set; }
        /// <summary>
        /// Адрес доставки
        /// </summary>
        [DisplayName("Адрес доставки")]
        public string ShipAddress { get; set; }
        /// <summary>
        /// Продавец
        /// </summary>
        [DisplayName("Продавец")]
        public ContrAgent SelledId { get; set; }
        /// <summary>
        /// Покупатель
        /// </summary>
        [DisplayName("Покупатель")]
        public ContrAgent ConsumerId { get; set; }
        /// <summary>
        /// Перевозчикк
        /// </summary>
        [DisplayName("Перевозчикк")]
        public ContrAgent ShipperId { get; set; }
        /// <summary>
        /// Водитель
        /// </summary>
        [DisplayName("Водитель")]
        public string Shippername { get; set; }
        /// <summary>
        /// Номер машины
        /// </summary>
        [DisplayName("Номер машины")]
        public string Carname { get; set; }
        /// <summary>
        /// Растояние
        /// </summary>
        [DisplayName("Растояние")]
        public  float Rasstoyanie { get; set; }
        /// <summary>
        /// Куплено
        /// </summary>
        [DisplayName("Куплено")]
        public float Price { get; set; }
        /// <summary>
        /// Доставлено
        /// </summary>
        [DisplayName("Доставлено")]
        public float ShipPrice { get; set; }
        /// <summary>
        /// Принято
        /// </summary>
        [DisplayName("Принято")]
        public float DonePrice { get; set; }
        /// <summary>
        /// Цена покупки с НДС
        /// </summary>
        [DisplayName("Цена покупки с НДС")]
        public float BuyPriceWithNDSForOneKG { get; set; }
        /// <summary>
        /// Цена продажи с НДС
        /// </summary>
        [DisplayName("Цена продажи с НДС")]
        public float SellPriceWithNDSForOneKG { get; set; }
        /// <summary>
        /// Цена доставки с НДС
        /// </summary>
        [DisplayName("Цена доставки с НДС")]
        public float ShipPriceWithNDSForOneKG { get; set; }
        /// <summary>
        /// Цена покупки с НДС
        /// </summary>
        [DisplayName("Цена хранения")]
        public float SafePriceForOneKG { get; set; }
        /// <summary>
        /// НДС продажи
        /// </summary>
        [DisplayName("НДС продажи")]
        public float SellPriceNDS { get; set; }
        /// <summary>
        /// НДС покупки
        /// </summary>
        [DisplayName("НДС покупки")]
        public float BuyPriceNDS { get; set; }
        /// <summary>
        /// НДС доставки
        /// </summary>
        [DisplayName("НДС доставки")]
        public float ShipPriceNDS { get; set; }
        /// <summary>
        /// НДС хранения
        /// </summary>
        [DisplayName("НДС хранения")]
        public float SafePriceNDS { get; set; }
        /// <summary>
        /// Налог на прибыль
        /// </summary>
        [DisplayName("Налог на прибыль")]
        public float MoneyNDS { get; set; }
        /// <summary>
        /// Дельта доставки
        /// </summary>
        [DisplayName("Дельта доставки")]
        public float DeltaShipping { get; set; }
        /// <summary>
        /// прощенная дельта доставки
        /// </summary>
        [DisplayName("Прощенная дельта доставки")]
        public float DeltaProshenaya { get; set; }
        /// <summary>
        /// ИТОГО дельта при доставке
        /// </summary>
        [DisplayName("ИТОГО дельта")]
        public float SummDeltaShipp { get; set; }
        /// <summary>
        /// компенсация за дельту
        /// </summary>
        [DisplayName("Компенсация за дельту")]
        public int DeltaCompinsation { get; set; }
        /// <summary>
        /// Цена за дельту
        /// </summary>
        [DisplayName("Цена за дельту")]
        public float DeltaPrice { get; set; }
        /// <summary>
        /// Дельта при приёмке
        /// </summary>
        [DisplayName("Дельта при приёмке")]
        public float SubmitDelta { get; set; }
        /// <summary>
        /// Схема расчета транспортных расходов
        /// </summary>
        [DisplayName("Схема расчета")]
        public TransoprtScheme transoprtScheme { get; set; }
        /// <summary>
        /// Стоимость доаставки с НДС
        /// </summary>
        [DisplayName("Стоимость доаставки с НДС")]
        public float ShipPriceWithNDS{ get; set; }
        /// <summary>
        /// Штраф за недоставленный товар
        /// </summary>
        [DisplayName("Штраф за недоставленный товар")]
        public float ShipSancForNum{ get; set; }
        /// <summary>
        /// Доп расходы на транспорт
        /// </summary>
        [DisplayName("Доп расходы на транспорт")]
        public float TransportdopRaschod { get; set; }
        /// <summary>
        /// Суммарная стоимость доставки с НДС
        /// </summary>
        [DisplayName("Суммарная стоимость доставки с НДС")]
        public float transoprtSummWithNDS { get; set; }
        /// <summary>
        /// НДС Доставки
        /// </summary>
        [DisplayName("НДС Доставки")]
        public float transoprtShipNDS { get;set;        }
        /// <summary>
        /// Стоимость доставки без НДС
        /// </summary>
        [DisplayName("Стоимость доставки без НДС")]
        public float transoprtShipWithOutNDS { get; set; }
        /// <summary>
        /// АКТ БУ номер
        /// </summary>
        [DisplayName("АКТ БУ номер")]
        public string ACTBUNumber { get; set; }
        /// <summary>
        /// АКТ БУ дата
        /// </summary>
        [DisplayName("АКТ БУ дата")]
        public DateTime ACTBUDate { get; set; }
        /// <summary>
        /// Тип ТТН
        /// </summary>
        [DisplayName("Схема расчета")]
        public string TTNType { get; set; }
        /// <summary>
        /// Оплата с НДС
        /// </summary>
        [DisplayName("Оплата с НДС")]
        public float transoprtPayWithNDS { get; set; }
        /// <summary>
        /// Схема расчета покупки
        /// </summary>
        [DisplayName("Схема расчета покупки")]
        public TransoprtScheme buyScheme { get; set; }
        /// <summary>
        /// Схема расчета покупки
        /// </summary>
        [DisplayName("Сумма покупки с НДС")]
        public float buyWithNDS { get; set; }
        /// <summary>
        /// Штраф за потрею при доставке
        /// </summary>
        [DisplayName("Штраф за потрею при доставке")]
        public float buySancForLost { get; set; }
        /// <summary>
        /// Покупка с учетом компенсации
        /// </summary>
        [DisplayName("Покупка с учетом компенсации")]
        public float buyWithCompensation { get; set; }
        /// <summary>
        /// Покупка без НДС
        /// </summary>
        [DisplayName("Покупка без НДС")]
        public float withOutNDS { get; set; }
        /// <summary>
        /// НДС Покупки
        /// </summary>
        [DisplayName("НДС Покупки")]
        public float buyNDS
        {
            get;set;
        }
        /// <summary>
        /// АКТ БУ Покупка
        /// </summary>
        [DisplayName("Номер акта БУ")]
        public string buyActBU { get; set; }
        /// <summary>
        /// АКТ БУ ПОКУПКА ДАТА
        /// </summary>
        [DisplayName("Дата акта БУ")]
        public DateTime buyActDate { get; set; }
        /// <summary>
        /// Оплата покупки с НДС
        /// </summary>
        [DisplayName("Оплата покупки с НДС")]
        public float buySummWithNDS { get; set; }
        #region CarantSertificates   
        /// <summary>
        /// Номер сертефиката
        /// </summary>
        [DisplayName("Номер карантийного сертефиката")]
        public string cSNumber { get; set; }
        /// <summary>
        /// Пункт отрузки
        /// </summary>
        [DisplayName("Пункт отрузки")]
        public string cSShipPoint { get; set; }
        /// <summary>
        /// Отгружено по сертефикату
        /// </summary>
        [DisplayName("Отгружено по сертефикату")]
        public float cSOtgruz { get; set; }
        /// <summary>
        /// Стоимость 1 кг по сертефикату без НДС
        /// </summary>
        [DisplayName("Стоимость 1 кг по сертефикату без НДС")]
        public float cSOneKGPriceWithoutNDS { get; set; }
        /// <summary>
        /// ИТОГО стоимость с карантийным
        /// </summary>
        [DisplayName("ИТОГО стоимость с карантийным")]
        public float cSSumm { get; set; }
        #endregion
        #region Chranenije
        /// <summary>
        /// Хранение - контрагент
        /// </summary>
        [DisplayName("Контрагент - хранение")]
        public ContrAgent safeContr{ get; set; }
        /// <summary>
        /// Стоимость хранение с НДС
        /// </summary>
        [DisplayName("Стоимость хранение с НДС")]
        public float safePriceWithNDS { get; set; }
        /// <summary>
        /// НДС Хранения
        /// </summary>
        [DisplayName("НДС Хранения")]
        public float safeNDSSUMM { get; set; }
        /// <summary>
        /// Стоимость хранения без НДС
        /// </summary>
        [DisplayName("Стоимость хранения без НДС")]
        public float safeSUMMWithOutNDS { get; set; }
        /// <summary>
        /// АКТ БУ Хранение
        /// </summary>
        [DisplayName("АКТ БУ Хранение")]
        public string safeActBU { get; set; }
        /// <summary>
        /// АКТ БУ Хранение ДАТА
        /// </summary>
        [DisplayName("АКТ БУ Хранение ДАТА")]
        public DateTime safeActDate { get; set; }
        #endregion
        #region DopRaschody
        /// <summary>
        /// Агентские расходы
        /// </summary>
        [DisplayName("Агентские расходы")]
        public float raschAgentskije { get; set; }
        /// <summary>
        /// Прочие расходы
        /// </summary>
        [DisplayName("Прочие расходы")]
        public float raschProchee { get; set; }
        /// <summary>
        /// ВСЕ Доп расходы
        /// </summary>
        [DisplayName("Сумма доп расходов")]
        public float raschSummAll { get;set;
        }
        #endregion

        #region Prodaja
        /// <summary>
        /// Выручка с НДС
        /// </summary>
        [DisplayName("Выручка с НДС")]
        public float prodWithNDS { get; set; }
        /// <summary>
        /// Выручка без НДС
        /// </summary>
        [DisplayName("Выручка без НДС")]
        public float prodWithOutNDS { get; set; }
        /// <summary>
        /// НДС продажи
        /// </summary>
        [DisplayName("НДС продажи")]
        public float prodNDSSumm { get; set; }
        /// <summary>
        /// Договор продажи
        /// </summary>
        [DisplayName("Договор продажи")]
        public string prodDogovor{ get; set; }
        /// <summary>
        /// АКТ БУ Продажа
        /// </summary>
        [DisplayName("АКТ БУ Продажа")]
        public string prodActBU { get; set; }
        /// <summary>
        /// АКТ БУ Продажа ДАТА
        /// </summary>
        [DisplayName("АКТ БУ Продажа ДАТА")]
        public DateTime prodActDate { get; set; }
        /// <summary>
        /// Оплата с НДС
        /// </summary>
        [DisplayName("Оплата с НДС")]
        public float prodPayWithNDS { get; set; }
        #endregion
        #region NakladnyjeRaschody
        /// <summary>
        /// Прямые расходы без НДС
        /// </summary>
        [DisplayName("Прямые расходы без НДС")]
        public float nakRaschWithOutNDS { get; set; }
        [DisplayName("база распределения по подразделениям")]
        public float bazaRaspredByDepartment { get; set; }
        [DisplayName("накладные подразделения без прочих доходов и НП")]
        public float nakRaschOfDepartmentWithOutNP { get; set; }
        [DisplayName("база распределения по всем сделкам")]
        public float bazaRaspredelenijaAllDeals { get; set; }
        [DisplayName("Затраты УК")]
        public float UKZatraty { get; set; }
        /// <summary>
        /// Накладные расходы
        /// </summary>
        [DisplayName("Накладные расходы")]
        public float nakRasch { get; set; }
        /// <summary>
        /// Суммарные накладные расходы
        /// </summary>
        [DisplayName("Суммарные накладные расходы")]
        public float nakRaschSUMM { get; set; }
        #endregion
        #region Marja
        /// <summary>
        /// Маржинальная прибыль
        /// </summary>
        [DisplayName("Маржинальная прибыль")]
        public float marjPribyl { get; set; }
        /// <summary>
        /// Маржинальность
        /// </summary>
        [DisplayName("Маржинальность")]
        public float marjiNalnost { get; set; }
        /// <summary>
        /// Маржа на 1 кг
        /// </summary>
        [DisplayName("Маржа на 1 кг")]
        public float marjaOneKg { get; set; }
        [DisplayName("маржинальная прибыль")]
        public float MPMinusNDSNP { get; set; }
        [DisplayName("Маржинальность")]
        public float marjiNalnostNP { get; set; }
        [DisplayName("маржа на 1 кг купленного товара")]
        public float marjaOneKgNP { get; set; }
        #endregion
        #region ChistayaPribyl
        /// <summary>
        /// Чистая прибыль до НП
        /// </summary>
        [DisplayName("Чистая прибыль до НП")]
        public float prybDoNP { get; set; }
        /// <summary>
        /// Налог на прибыль
        /// </summary>
        [DisplayName("Налог на прибыль")]
        public float nalogNP { get; set; }
        /// <summary>
        /// Чистая прибыль
        /// </summary>
        [DisplayName("Чистая прибыль")]
        public float chistPryb { get; set; }
        /// <summary>
        /// Чистая прибыль на 1 кг
        /// </summary>
        [DisplayName("Чистая прибыль на 1 кг")]
        public float chistPrybOneKG { get; set; }
        /// <summary>
        /// Рентабельность
        /// </summary>
        [DisplayName("Рентабельность")]
        public float chistPrybRenta { get; set; }
        #endregion
        #region AnalizPoter
        /// <summary>
        /// Затраты при потере при доставке без компенсации
        /// </summary>
        [DisplayName("Затраты при потере при доставке без компенсации")]
        public float poteriPriDostWithOutCompensation { get; set; }
        /// <summary>
        /// Недополученная прибыль при доставке
        /// </summary>
        [DisplayName("Недополученная прибыль при доставке")]
        public float poteriPribPriDostWithOutCompensation { get; set; }
        /// <summary>
        /// Затраты при потере при приемке
        /// </summary>
        [DisplayName("Затраты при потере при приемке")]
        public float poteriDostavPrijem { get; set; }
        /// <summary>
        /// Недополученная прибыль при приемке
        /// </summary>
        [DisplayName("Недополученная прибыль при приемке")]
        public float poteriPryb { get; set; }
        /// <summary>
        /// Чистая прибыль с учетмо недополученной прибыли
        /// </summary>
        [DisplayName("Чистая прибыль с учетом недополученной прибыли")]
        public float chistPrybnedPryb { get; set; }
        /// <summary>
        /// Прибыль на 1 кг
        /// </summary>
        [DisplayName("Прибыль на 1 кг")]
        public float chistPrybOneKGFinish { get; set; }
        /// <summary>
        /// Конечная рентабельность
        /// </summary>
        [DisplayName("Рентабельность")]
        public float finishRenta { get; set; }
        #endregion
        public Cash SellerCash { get; set; }
        public Cash ShipperCash { get; set; }
        public Cash BuyerCash { get; set; }
        
        float CalculateWithNDS(float value, float nds) {
            return value / (nds / 100 + 1);
        }


        Deal CalculateParent(Deal deal) {

            return deal;
        }
    }
}
