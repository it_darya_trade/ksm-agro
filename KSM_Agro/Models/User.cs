﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace KSM_Agro.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public int RegID { get; set; }
        public string Token { get; set; }
        public int UserRole { get; set; }
        [ForeignKey("UserDepartment")]
        public Department Department { get; set; }
        [ForeignKey("UserDeals")]
        public List<Deal> deals { get; set; }
        [ForeignKey("UserPosts")]
        public List<Post> posts { get; set; }

    }
}
