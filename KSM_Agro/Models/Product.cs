﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KSM_Agro.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        [Display(Name ="Наименование")]
        public string ProductTitle { get; set; }
        [Display(Name = "Цена")]
        [Column(TypeName = "float")]
        public double ProductPrice { get; set; }
        [Display(Name = "Цена с НДС")]
        [Column(TypeName ="float")]
        public double ProductsPriceWithNDS { get; set; }
    }
}
