﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSM_Agro.Models
{
    public class Post
    {
        public int PostID { get; set; }
        public string PostTitle { get; set; }
        public string PostImage { get; set; }
        public string PostDescription { get; set; }
        public string PostText { get; set; }
        public DateTime PostDS { get; set; }
        public string PostKeyWords { get; set; }
        public string PostMetaDesc { get; set; }
        public User PostedBy { get; set; }
    }
}
