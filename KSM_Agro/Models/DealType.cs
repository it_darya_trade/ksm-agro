﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSM_Agro.Models
{
    public class DealType
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
