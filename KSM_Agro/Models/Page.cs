﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSM_Agro.Models
{
    public class Page
    {
        public int PageID { get; set; }
        public string PageTitle { get; set; }
        public string PageUrl { get; set; }
        public string PageDescription { get; set; }
        public string PageImage { get; set; }
        public string PageSource { get; set; }
        public string PageKeywords { get; set; }
    }
}
