﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KSM_Agro.Models
{
    public class Department
    {
        public int DepartmentID { get; set; }
        [Display(Name="Наименование")]
        public string DepartmentTitle { get; set; }
        [Display(Name = "ИНН")]
        public string DepartmentInn { get; set; }
        [Display(Name = "КПП")]
        public string DepartmentKpp { get; set; }
        [Display(Name = "ОГРН")]
        public string DepartmentOgrn { get; set; }
        [Display(Name = "Адресс")]
        public string DepartmentAddress { get; set; }
        [Display(Name = "Банк")]
        public string DepartmentBank { get; set; }
        [Display(Name = "БИК")]
        public string DepartmentBik { get; set; }
        [Display(Name = "Руководитель")]
        public User DepartmentCEO { get; set; }
    }
}
