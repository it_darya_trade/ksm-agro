﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace KSM_Agro.Models
{
    public class ContrAgent
    {
        
        public int ContrAgentID { get; set; }
        [Display(Name="Наименование")]
        public string ContrAgent_Name { get; set; }
        [Display(Name= "ИНН")]
        public string ContrAgent_Inn { get; set; }
        [Display(Name= "КПП")]
        public string ContrAgent_Kpp { get; set; }
        [Display(Name= "ОГРН")]
        public string ContrAgent_Ogrn { get; set; }
        public string ContrAgent_Contacts { get; set; }
        public string ContrAgent_Address { get; set; }
        public string ContrAgent_CEO { get; set; }
    }
}
